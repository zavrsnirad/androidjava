package com.example.android.Util;

import android.util.Base64;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.UnsupportedEncodingException;

class JsonWebToken {

    static JsonObject decoded(String JWTEncoded) throws Exception {
        String[] split = JWTEncoded.split("\\.");
        String decodedJWT = getJson(split[1]);
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(decodedJWT);
        return jsonElement.getAsJsonObject();
    }

    private static String getJson(String stringEncoded) throws UnsupportedEncodingException {
        byte[] decodedBytes = Base64.decode(stringEncoded, Base64.URL_SAFE);
        return new String(decodedBytes, "UTF-8");
    }
}
