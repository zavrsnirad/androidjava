package com.example.android.Util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.android.Common.SharedPrefCommon;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

@SuppressLint("Registered")
public class SharedPref extends SharedPrefCommon {

    public void saveLoginData(Context context, String token, String refreshToken) {
        try {
            JsonObject jwtObject = JsonWebToken.decoded(token);
            SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(USER_TOKEN, token);
            editor.putString(USER_REFRESH_TOKEN, refreshToken);
            editor.putString(USERNAME, jwtObject.get("username").getAsString());
            editor.putInt(ISSUED_AT, jwtObject.get("iat").getAsInt());
            editor.putInt(EXPIRATION_TIME, jwtObject.get("exp").getAsInt());
            editor.putBoolean(IS_USER_LOGGED_IN, true);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void logoutUser(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(USER_TOKEN);
        editor.remove(USER_REFRESH_TOKEN);
        editor.remove(USERNAME);
        editor.remove(ISSUED_AT);
        editor.remove(EXPIRATION_TIME);
        editor.remove(IS_USER_LOGGED_IN);
        editor.putBoolean(IS_USER_LOGGED_IN, false);
        editor.apply();
    }

    public String getUsername(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        return sharedPreferences.getString(USERNAME, null);
    }

    public boolean isUserLoggedIn(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        int currentTime = (int) (System.currentTimeMillis() / 1000);
        int expirationTime = sharedPreferences.getInt(EXPIRATION_TIME, 0);
        if (currentTime > expirationTime) {
            editor.putBoolean(IS_USER_LOGGED_IN, false);
            editor.apply();
            return false;
        }

        return sharedPreferences.getBoolean(IS_USER_LOGGED_IN, false);
    }

    public String getUserToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPref.SHARED_PREFS, MODE_PRIVATE);
        return "BEARER " + sharedPreferences.getString(USER_TOKEN, null);
    }

    public void saveNewToken(Context context, String token) {
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPref.SHARED_PREFS, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            String currentToken = sharedPreferences.getString(USER_TOKEN, null);
            if (currentToken != null) {
                editor.remove(USER_TOKEN);
                editor.remove(USERNAME);
                editor.remove(ISSUED_AT);
                editor.remove(EXPIRATION_TIME);
            }
            JsonObject jwtObject = JsonWebToken.decoded(token);
            editor.putString(USER_TOKEN, token);
            editor.putString(USERNAME, jwtObject.get("username").getAsString());
            editor.putInt(ISSUED_AT, jwtObject.get("iat").getAsInt());
            editor.putInt(EXPIRATION_TIME, jwtObject.get("exp").getAsInt());
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isEnabledDailyNotification(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPref.SHARED_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(IS_ENABLED_DAILY_NOTIFICATION, false);
    }

    public void setEnabledDailyNotification(Context context, boolean isEnabled) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPref.SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_ENABLED_DAILY_NOTIFICATION, isEnabled);
        editor.apply();
    }

    @SuppressLint("SimpleDateFormat")
    public void setSentMail(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPref.SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d.M.Y");
        String date = dateFormat.format(calendar.getTime());
        editor.putString(DATE_MAIL_SEND, date);
        editor.apply();
    }

    @SuppressLint("SimpleDateFormat")
    public boolean isMailSentToday(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPref.SHARED_PREFS, MODE_PRIVATE);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d.M.Y");
        String date = dateFormat.format(calendar.getTime());
        return date.equals(sharedPreferences.getString(DATE_MAIL_SEND, ""));
    }
}
