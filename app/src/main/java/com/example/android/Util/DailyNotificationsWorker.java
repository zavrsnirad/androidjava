package com.example.android.Util;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.android.API.Request.RestDayInterface;
import com.example.android.API.Response.RestDay;
import com.example.android.API.RetrofitApi;
import com.example.android.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.example.android.App.DAILY_CHANNEL_ID;
import static com.example.android.Common.ResponseTypes.ERROR_ACTIVE_REST_DAY;
import static com.example.android.Common.ResponseTypes.SUCCESS_ACTIVE_REST_DAY;
import static com.example.android.Common.SharedPrefCommon.DAILY_NOTIFICATION_DATE;
import static com.example.android.Common.SharedPrefCommon.SHARED_PREFS;

public class DailyNotificationsWorker extends Worker {

    private SharedPref sharedPref = new SharedPref();
    private RestDayInterface restDayApiClient = RetrofitApi.getRestDayApi();
    private Call<RestDay> restDayCall = restDayApiClient.getActiveRestDay(sharedPref.getUserToken(getApplicationContext()));

    public DailyNotificationsWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        if (sharedPref.isUserLoggedIn(getApplicationContext()) &&
                sharedPref.isEnabledDailyNotification(getApplicationContext()) &&
                isAllowedNotificationTime() &&
                !isNotificationShownToday()) {
            saveNotificationShown();
            createNotification(restDayCall);
        }
        return Result.success();
    }

    private void displayNotification(String title, String text) {
        long[] pattern = {100, 100, 100, 100, 100, 100, 100};
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
        Notification notification = new NotificationCompat.Builder(getApplicationContext(), DAILY_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_fitness)
                .setContentTitle(title)
                .setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_REMINDER)
                .setVibrate(pattern)
                .build();
        notificationManagerCompat.notify(1, notification);
    }

    private void createNotification(Call<RestDay> call) {
        call.enqueue(new Callback<RestDay>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<RestDay> call, Response<RestDay> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_ACTIVE_REST_DAY:
                                if (isRestDayToday(response.body().getRestDayData().getRestDay())) {
                                    displayNotification("Rest day", "Hope you had a good rest today!");
                                } else {
                                    displayNotification("Workout day", "Did you do your workout today?");
                                }
                                break;
                            case ERROR_ACTIVE_REST_DAY:
                                displayNotification("Rest day", "You didn't save your rest day! Please do that!");
                                saveNotificationShown();
                                break;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RestDay> call, Throwable t) {

            }
        });
    }

    private boolean isRestDayToday(String restDay) {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String today = null;

        switch (day) {
            case Calendar.MONDAY:
                today = "Monday";
                break;
            case Calendar.TUESDAY:
                today = "Tuesday";
                break;
            case Calendar.WEDNESDAY:
                today = "Wednesday";
                break;
            case Calendar.THURSDAY:
                today = "Thursday";
                break;
            case Calendar.FRIDAY:
                today = "Friday";
                break;
            case Calendar.SATURDAY:
                today = "Saturday";
                break;
            case Calendar.SUNDAY:
                today = "Sunday";
                break;
        }

        return today.equals(restDay);
    }

    @SuppressLint("SimpleDateFormat")
    private boolean isAllowedNotificationTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String currentTime = dateFormat.format(calendar.getTime());
        return currentTime.compareTo("18:00:00") > 0;
    }

    @SuppressLint("SimpleDateFormat")
    private boolean isNotificationShownToday() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d.M.Y");
        String currentDate = dateFormat.format(calendar.getTime());
        return currentDate.equals(sharedPreferences.getString(DAILY_NOTIFICATION_DATE, ""));
    }

    @SuppressLint("SimpleDateFormat")
    private String getTodayDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d.M.Y");
        return dateFormat.format(calendar.getTime());
    }

    private void saveNotificationShown() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(DAILY_NOTIFICATION_DATE, getTodayDate());
        editor.apply();
    }
}
