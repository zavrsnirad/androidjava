package com.example.android.Util;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.android.API.ResReq.UserFactory;

import static android.content.Context.MODE_PRIVATE;
import static com.example.android.Common.SharedPrefCommon.SHARED_PREFS;
import static com.example.android.Common.SharedPrefCommon.USER_REFRESH_TOKEN;

public class RefreshTokenWorker extends Worker {

    private UserFactory userFactory = new UserFactory();
    private SharedPref sharedPref = new SharedPref();

    public RefreshTokenWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        if (sharedPref.isUserLoggedIn(getApplicationContext())) {
            userFactory.refreshToken(getApplicationContext(), sharedPreferences.getString(USER_REFRESH_TOKEN, null));
        }
        return Result.success();
    }
}
