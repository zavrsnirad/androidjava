package com.example.android.DataModel;

public class ImagesOnHome {
    private final Integer userId;
    private final String username;
    private final String profileImage;
    private final String image;
    private final String description;

    public ImagesOnHome(Integer userId, String username, String profileImage, String image, String description) {
        this.userId = userId;
        this.username = username;
        this.profileImage = profileImage;
        this.image = image;
        this.description = description;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public String getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }
}
