package com.example.android.DataModel;

public class UsersInSearch {
    private final Integer userId;
    private final String username;
    private final String profileImage;

    public UsersInSearch(Integer userId, String username, String profileImage) {
        this.userId = userId;
        this.username = username;
        this.profileImage = profileImage;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getProfileImage() {
        return profileImage;
    }
}
