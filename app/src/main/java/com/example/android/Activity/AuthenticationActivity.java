package com.example.android.Activity;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;


import com.example.android.Fragment.LoginFragment_;
import com.example.android.Fragment.RegisterFragment_;
import com.example.android.R;
import com.example.android.Util.SharedPref;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@SuppressLint("Registered")
@EActivity(R.layout.authentication_activity)
public class AuthenticationActivity extends FragmentActivity {

    @ViewById
    BottomNavigationView bottomAuthenticationNavigation;

    ArrayList<String> backList = new ArrayList<>();
    FragmentManager fragmentManager = AuthenticationActivity.this.getSupportFragmentManager();
    String tag = null;
    int currentItem = 0;

    @AfterViews
    void init() {
        backList.add(0, "default");
        SharedPref sharedPref = new SharedPref();
        if (sharedPref.isUserLoggedIn(this)) {
            AuthenticatedActivity_.intent(this).start();
            finish();
        }
        currentItem = bottomAuthenticationNavigation.getMenu().getItem(0).getItemId();
        bottomAuthenticationNavigation.setOnNavigationItemSelectedListener(listener);
        fragmentManager.beginTransaction().addToBackStack("login").replace(R.id.fragment_container,
                new LoginFragment_(), "login").commit();
        fragmentManager.executePendingTransactions();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener listener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            if (currentItem == menuItem.getItemId()) {
                return false;
            }

            ArrayList<String> tagList = new ArrayList<>();
            Fragment selectedFragment = null;
            if (tag != null) {
                backList.add(tag);
            } else {
                backList.add("login");
            }

            switch (menuItem.getItemId()) {
                case R.id.nav_login:
                    currentItem = R.id.nav_login;
                    selectedFragment = new LoginFragment_();
                    tag = "login";
                    break;
                case R.id.nav_register:
                    currentItem = R.id.nav_register;
                    selectedFragment = new RegisterFragment_();
                    tag = "register";
                    break;
            }

            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
                tagList.add(fragmentManager.getBackStackEntryAt(i).getName());
            }

            if (tagList.contains(tag)) {
                int index = tagList.lastIndexOf(tag);
                FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(index);
                String fragmentTag = backStackEntry.getName();
                Fragment fragment = fragmentManager.findFragmentByTag(fragmentTag);
                if (fragment != null) {
                    fragmentManager.beginTransaction().addToBackStack(tag).replace(R.id.fragment_container,
                            fragment, tag).commit();
                    fragmentManager.executePendingTransactions();
                    return true;
                }
            }

            if (selectedFragment != null) {
                fragmentManager.beginTransaction().addToBackStack(tag).replace(R.id.fragment_container,
                        selectedFragment, tag).commit();
                fragmentManager.executePendingTransactions();
            }

            return true;
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        String backAction = backList.get(backList.size() - 1);
        if (backAction != null) {
            switch (backAction) {
                case "login":
                    currentItem = R.id.nav_login;
                    bottomAuthenticationNavigation.getMenu().getItem(0).setChecked(true);
                    break;
                case "register":
                    currentItem = R.id.nav_register;
                    bottomAuthenticationNavigation.getMenu().getItem(1).setChecked(true);
                    break;
                case "default":
                    finish();
            }
            backList.remove(backList.size() - 1);
        }
    }
}
