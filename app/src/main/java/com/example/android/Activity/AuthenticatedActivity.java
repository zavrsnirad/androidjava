package com.example.android.Activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.example.android.Fragment.CalendarFragment_;
import com.example.android.Fragment.HomeFragment_;
import com.example.android.Fragment.ImageUploadFragment_;
import com.example.android.Fragment.ProfileFragment_;
import com.example.android.Fragment.RestDayFragment_;
import com.example.android.R;
import com.example.android.Util.DailyNotificationsWorker;
import com.example.android.Util.RefreshTokenWorker;
import com.example.android.Util.SharedPref;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@SuppressLint("Registered")
@EActivity(R.layout.authenticated_activity)
public class AuthenticatedActivity extends FragmentActivity {

    @ViewById
    public BottomNavigationView bottomAuthenticatedNavigation;

    public ArrayList<Integer> backList = new ArrayList<>();
    private FragmentManager fragmentManager = AuthenticatedActivity.this.getSupportFragmentManager();
    private String tag = null;
    int currentItem = 0;

    @AfterViews
    void init() {
        backList.add(0, 0);
        SharedPref sharedPref = new SharedPref();
        if (!sharedPref.isUserLoggedIn(this)) {
            AuthenticationActivity_.intent(this).start();
            finish();
        }

        bottomAuthenticatedNavigation.setOnNavigationItemSelectedListener(listener);
        bottomAuthenticatedNavigation.getMenu().getItem(2).setChecked(true);
        fragmentManager.beginTransaction().addToBackStack("home").replace(R.id.fragment_container_authenticated,
                new HomeFragment_(), "home").commit();
        currentItem = bottomAuthenticatedNavigation.getMenu().getItem(2).getItemId();
        WorkManager instance = WorkManager.getInstance(getApplicationContext());
        PeriodicWorkRequest refreshRequest = new PeriodicWorkRequest.Builder(RefreshTokenWorker.class, 4, TimeUnit.HOURS).build();
        PeriodicWorkRequest notificationRequest = new PeriodicWorkRequest.Builder(DailyNotificationsWorker.class, 15, TimeUnit.MINUTES).build();
        instance.enqueue(refreshRequest);
        instance.enqueue(notificationRequest);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener listener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            if (currentItem == menuItem.getItemId()) {
                return false;
            }

            ArrayList<String> tagList = new ArrayList<>();
            Fragment selectedFragment = null;

            backList.add(currentItem);

            switch (menuItem.getItemId()) {
                case R.id.nav_profile:
                    currentItem = R.id.nav_profile;
                    selectedFragment = new ProfileFragment_();
                    tag = "profile";
                    break;
                case R.id.nav_image_upload:
                    currentItem = R.id.nav_image_upload;
                    selectedFragment = new ImageUploadFragment_();
                    tag = "image";
                    break;
                case R.id.nav_home:
                    currentItem = R.id.nav_home;
                    selectedFragment = new HomeFragment_();
                    tag = "home";
                    break;
                case R.id.nav_calendar:
                    currentItem = R.id.nav_calendar;
                    selectedFragment = new CalendarFragment_();
                    tag = "calendar";
                    break;
                case R.id.nav_rest_day:
                    currentItem = R.id.nav_rest_day;
                    selectedFragment = new RestDayFragment_();
                    tag = "restDay";
                    break;
            }

            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
                tagList.add(fragmentManager.getBackStackEntryAt(i).getName());
            }

            if (tagList.contains(tag)) {
                int index = tagList.lastIndexOf(tag);
                FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(index);
                String fragmentTag = backStackEntry.getName();
                selectedFragment = fragmentManager.findFragmentByTag(fragmentTag);
                if (selectedFragment != null) {
                    Bundle arguments = new Bundle();
                    arguments.putBoolean("fromBackStack", true);
                    selectedFragment.setArguments(arguments);
                    fragmentManager.beginTransaction().addToBackStack(tag).replace(R.id.fragment_container_authenticated,
                            selectedFragment, tag).commit();
                    fragmentManager.executePendingTransactions();
                    return true;
                }
            }

            if (selectedFragment != null) {
                fragmentManager.beginTransaction().addToBackStack(tag).replace(R.id.fragment_container_authenticated,
                        selectedFragment, tag).commit();
                fragmentManager.executePendingTransactions();
            }

            return true;
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Integer backAction = backList.get(backList.size() - 1);
        if (backAction != null) {
            switch (backAction) {
                case R.id.nav_profile:
                    currentItem = R.id.nav_profile;
                    bottomAuthenticatedNavigation.getMenu().getItem(0).setChecked(true);
                    break;
                case R.id.nav_image_upload:
                    currentItem = R.id.nav_image_upload;
                    bottomAuthenticatedNavigation.getMenu().getItem(1).setChecked(true);
                    break;
                case R.id.nav_home:
                    currentItem = R.id.nav_home;
                    bottomAuthenticatedNavigation.getMenu().getItem(2).setChecked(true);
                    break;
                case R.id.nav_calendar:
                    currentItem = R.id.nav_calendar;
                    bottomAuthenticatedNavigation.getMenu().getItem(3).setChecked(true);
                    break;
                case R.id.nav_rest_day:
                    currentItem = R.id.nav_rest_day;
                    bottomAuthenticatedNavigation.getMenu().getItem(4).setChecked(true);
                    break;
                case 0:
                    finish();
                    break;
            }
            backList.remove(backList.size() - 1);
        }
    }
}
