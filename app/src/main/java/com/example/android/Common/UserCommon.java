package com.example.android.Common;

import android.annotation.SuppressLint;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressLint("Registered")
public class UserCommon {
    public String getUsername(EditText username) {
        return username.getText().toString().trim();
    }

    public String getEmail(EditText email) {
        return email.getText().toString().trim();
    }

    public String getPassword(EditText password) {
        return password.getText().toString().trim();
    }

    public String getConfirmPassword(EditText confirmPassword) {
        return confirmPassword.getText().toString().trim();
    }

    public boolean arePasswordsValid(String password, String confirmPassword) {
        return password.equals(confirmPassword);
    }

    public boolean isUsernameValid(String username) {
        return username.length() >= 6;
    }

    public boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    public boolean isConfirmPasswordValid(String confirmPassword) {
        return confirmPassword.length() >= 8;
    }

    public boolean isEmailValid(EditText editEmail) {
        String regex = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(getEmail(editEmail));
        return matcher.matches();
    }
}
