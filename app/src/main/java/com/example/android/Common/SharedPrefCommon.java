package com.example.android.Common;

import android.annotation.SuppressLint;

@SuppressLint("Registered")
public class SharedPrefCommon {

    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String USER_TOKEN = "userToken";
    public static final String USER_REFRESH_TOKEN = "userRefreshToken";
    public static final String IS_USER_LOGGED_IN = "isUserLoggedIn";
    public static final String ISSUED_AT = "issuedAt";
    public static final String EXPIRATION_TIME = "expirationTime";
    public static final String USERNAME = "username";
    public static final String DAILY_NOTIFICATION_DATE = "dailyNotificationDate";
    public static final String IS_ENABLED_DAILY_NOTIFICATION = "isEnabledDailyNotification";
    public static final String DATE_MAIL_SEND = "dateMailSend";
}
