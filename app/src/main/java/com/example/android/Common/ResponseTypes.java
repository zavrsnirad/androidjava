package com.example.android.Common;

public class ResponseTypes {
    public final static String SUCCESS_ACTIVE_REST_DAY = "successActiveRestDay";
    public final static String SUCCESS_REST_DAY = "successRestDay";
    public final static String SUCCESS_REST_DAYS = "successRestDays";
    public final static String SUCCESS_WORKOUT_DAY = "successWorkoutDay";
    public static final String SUCCESS_GET_USER_IMAGES = "successGetUserImages";
    public static final String SUCCESS_USER_DATA = "successUserData";
    public static final String SUCCESS_IMAGE_UPLOAD = "successImageUpload";
    public static final String SUCCESS_REGISTER = "successRegister";
    public static final String SUCCESS_TODAY_WORKOUT = "successTodayWorkout";
    public static final String SUCCESS_GET_USER_CALENDAR = "successGetUserCalendar";
    public static final String SUCCESS_USER_SEARCH = "successUserSearch";
    public static final String SUCCESS_USER_FOLLOWING = "successUserFollowing";
    public static final String SUCCESS_USER_FOLLOWERS = "successUserFollowers";
    public static final String SUCCESS_USER_FOLLOWED = "successUserFollowed";
    public static final String SUCCESS_USER_UNFOLLOWED = "successUserUnfollowed";
    public static final String SUCCESS_HOME_IMAGES = "successHomeImages";
    public static final String SUCCESS_USER_PROFILE_IMAGE = "successUserProfileImage";
    public static final String SUCCESS_IMAGE_DELETED = "successImageDeleted";

    public final static String ERROR_ACTIVE_REST_DAY = "errorActiveRestDay";
    public final static String ERROR_REST_DAY = "errorRestDay";
    public final static String ERROR_REST_DAYS = "errorRestDays";
    public final static String ERROR_WORKOUT_DAY = "errorWorkoutDay";
    public static final String ERROR_GET_USER_IMAGES = "errorGetUserImages";
    public static final String ERROR_USER_DATA = "errorUserData";
    public static final String ERROR_IMAGE_UPLOAD = "errorImageUpload";
    public static final String ERROR_REGISTER = "errorRegister";
    public static final String ERROR_TODAY_WORKOUT = "errorTodayWorkout";
    public static final String ERROR_GET_USER_CALENDAR = "errorGetUserCalendar";
    public static final String ERROR_USER_SEARCH = "errorUserSearch";
    public static final String ERROR_USER_FOLLOWING = "errorUserFollowing";
    public static final String ERROR_USER_FOLLOWERS = "errorUserFollowers";
    public static final String ERROR_USER_FOLLOWED = "errorUserFollowed";
    public static final String ERROR_USER_UNFOLLOWED = "errorUserUnfollowed";
    public static final String ERROR_HOME_IMAGES = "errorHomeImages";
    public static final String ERROR_USER_PROFILE_IMAGE = "errorUserProfileImage";
    public static final String ERROR_IMAGE_NOT_FOUND = "errorImageNotFound";
    public static final String ERROR_IMAGE_NOT_DELETED = "errorImageNotDeleted";

    public static final String MAIL_SENT = "mailSent";
}
