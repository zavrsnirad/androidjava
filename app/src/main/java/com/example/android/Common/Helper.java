package com.example.android.Common;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.android.Fragment.ProfileFragment_;
import com.example.android.Fragment.SingleImageFragment_;
import com.example.android.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.tapadoo.alerter.Alerter;

public class Helper {

    public void showAlert(Context context, String title, String text, int icon) {
        if (icon == 0) {
            icon = R.drawable.alerter_ic_notifications;
        }

        Alerter.create((Activity) context)
                .enableProgress(true)
                .enableSwipeToDismiss()
                .setDuration(2500)
                .setTitle(title)
                .setText(text)
                .setIcon(icon)
                .setIconColorFilter(0)
                .setBackgroundColorRes(R.color.alerterWhite)
                .setProgressColorRes(R.color.alerterSuccess)
                .setTextAppearance(R.style.alerterBlackText)
                .setProgressColorRes(R.color.alerterSuccess)
                .show();
    }

    public void openRestDayFragment(BottomNavigationView bottomNavigationView, Context context, Fragment fragment, Integer menuItem) {
        FragmentActivity activity = (FragmentActivity) context;
        FragmentManager manager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.addToBackStack("calendarToRestDay").replace(R.id.fragment_container_authenticated, fragment, "calendarToRestDay").commit();
        if (menuItem != null) {
            bottomNavigationView.getMenu().getItem(menuItem).setChecked(true);
        }
    }

    public void openSingleImageFragment(Context context, String imageFileName, String description) {
        Bundle bundle = new Bundle();
        bundle.putString("imageFileName", imageFileName);
        bundle.putString("description", description);
        FragmentActivity activity = (FragmentActivity) context;
        FragmentManager manager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        SingleImageFragment_ uploadedImageFragment = new SingleImageFragment_();
        uploadedImageFragment.setArguments(bundle);
        transaction.replace(R.id.fragment_container_authenticated, uploadedImageFragment).commit();
    }

    public void openProfileFragment(Context context, String username, int userId) {
        Bundle bundle = new Bundle();
        bundle.putString("username", username);
        bundle.putInt("userId", userId);
        FragmentActivity activity = (FragmentActivity) context;
        FragmentManager manager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        ProfileFragment_ profileFragment = new ProfileFragment_();
        profileFragment.setArguments(bundle);
        transaction.addToBackStack("homeToProfile").replace(R.id.fragment_container_authenticated, profileFragment, "homeToProfile").commit();
    }

    public int getDisplayHeight(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }
}
