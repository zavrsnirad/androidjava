package com.example.android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;

import java.util.ArrayList;

public class RestDayRecyclerListAdapter extends RecyclerView.Adapter<RestDayRecyclerListAdapter.ViewHolder> {

    private ArrayList<String> dayList;
    private ArrayList<String> activeFromList;
    private ArrayList<String> activeToList;
    private LayoutInflater layoutInflater;

    public RestDayRecyclerListAdapter(Context context, ArrayList<String> dayList, ArrayList<String> activeFromList, ArrayList<String> activeToList) {
        this.dayList = dayList;
        this.activeFromList = activeFromList;
        this.activeToList = activeToList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.rest_day_expandable_list_view_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String active = activeFromList.get(position) + " - " + activeToList.get(position);
        holder.itemDay.setText(dayList.get(position));
        holder.itemActive.setText(active);
    }

    @Override
    public int getItemCount() {
        return dayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemDay;
        TextView itemActive;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            itemDay = itemView.findViewById(R.id.item_day);
            itemActive = itemView.findViewById(R.id.item_active);
        }
    }
}
