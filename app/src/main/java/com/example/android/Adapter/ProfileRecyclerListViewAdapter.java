package com.example.android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.android.API.Request.ImageInterface;
import com.example.android.API.Response.Delete;
import com.example.android.API.RetrofitApi;
import com.example.android.Common.Helper;
import com.example.android.R;
import com.example.android.Util.SharedPref;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.android.Common.ResponseTypes.ERROR_IMAGE_NOT_DELETED;
import static com.example.android.Common.ResponseTypes.ERROR_IMAGE_NOT_FOUND;
import static com.example.android.Common.ResponseTypes.SUCCESS_IMAGE_DELETED;

public class ProfileRecyclerListViewAdapter extends RecyclerView.Adapter<ProfileRecyclerListViewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<String> imagePaths;
    private ArrayList<String> deletePaths;
    private String profileImagePath;
    private ArrayList<String> descriptionList;
    private String username;
    private LayoutInflater layoutInflater;
    private Call<Delete> deleteImageCall;
    private ImageInterface imageApiClient = RetrofitApi.getImageApi();
    private SharedPref sharedPref = new SharedPref();
    private Helper helper = new Helper();
    private boolean isMyProfile;
    private int imageDimension;
    private int profileDimension;

    public ProfileRecyclerListViewAdapter(
            Context context,
            ArrayList<String> imagePaths,
            ArrayList<String> deletePaths,
            ArrayList<String> descriptionList,
            String profileImagePath,
            String username,
            boolean isMyProfile
    ) {
        this.context = context;
        this.imagePaths = imagePaths;
        this.deletePaths = deletePaths;
        this.profileImagePath = profileImagePath;
        this.descriptionList = descriptionList;
        this.username = username;
        this.layoutInflater = LayoutInflater.from(context);
        this.isMyProfile = isMyProfile;
        this.imageDimension = (int) (helper.getDisplayHeight(context) * 0.55);
        this.profileDimension = (int) (helper.getDisplayHeight(context) * 0.05);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.profile_recycler_list_view_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Glide.with(context).load(imagePaths.get(position)).apply(new RequestOptions().override(imageDimension, imageDimension)).into(holder.profileImageItem);
        if (profileImagePath != null) {
            Glide.with(context).load(profileImagePath).apply(new RequestOptions().override(profileDimension, profileDimension)).into(holder.profileProfileImage);
        } else {
            Glide.with(context).load(R.mipmap.ic_profile_circle).apply(new RequestOptions().override(profileDimension, profileDimension)).into(holder.profileProfileImage);
        }
        holder.profileUsername.setText(username);
        holder.profileImageDescription.setText(descriptionList.get(position));
        holder.dotMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PopupMenu popup = new PopupMenu(context, holder.dotMenu);
                popup.inflate(R.menu.image_menu);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.delete_image:
                                deleteImageCall = imageApiClient.deleteImage(sharedPref.getUserToken(context), deletePaths.get(position));
                                deleteImageCall.enqueue(new Callback<Delete>() {
                                    @Override
                                    public void onResponse(Call<Delete> call, Response<Delete> response) {
                                        if (response.isSuccessful()) {
                                            if (response.body() != null) {
                                                switch (response.body().getType()) {
                                                    case SUCCESS_IMAGE_DELETED:
                                                        helper.showAlert(context, null, response.body().getMessage(), 0);
                                                        removeAt(position);
                                                        break;
                                                    case ERROR_IMAGE_NOT_FOUND:
                                                    case ERROR_IMAGE_NOT_DELETED:
                                                        helper.showAlert(context, null, response.body().getMessage(), 0);
                                                        break;
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Delete> call, Throwable t) {

                                    }
                                });

                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagePaths.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView profileImageItem;
        CircleImageView profileProfileImage;
        TextView profileUsername;
        TextView profileImageDescription;
        Button dotMenu;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            profileImageItem = itemView.findViewById(R.id.profile_image_item);
            profileProfileImage = itemView.findViewById(R.id.profile_profile_image);
            profileUsername = itemView.findViewById(R.id.profile_username);
            profileImageDescription = itemView.findViewById(R.id.profile_image_description);
            dotMenu = itemView.findViewById(R.id.dotMenu);
            if (!isMyProfile) {
                dotMenu.setVisibility(View.GONE);
            }
        }
    }

    private void removeAt(int position) {
        imagePaths.remove(position);
        deletePaths.remove(position);
        descriptionList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, imagePaths.size());
        notifyItemRangeChanged(position, deletePaths.size());
    }
}
