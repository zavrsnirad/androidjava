package com.example.android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.android.Common.Helper;
import com.example.android.R;

import java.util.ArrayList;

public class ProfileRecyclerGridViewAdapter extends RecyclerView.Adapter<ProfileRecyclerGridViewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<String> imagePaths;
    private LayoutInflater layoutInflater;
    private OnImageListener onImageListener;
    private int imageDimension;
    private Helper helper = new Helper();

    public ProfileRecyclerGridViewAdapter(Context context, ArrayList<String> imagePaths, OnImageListener onImageListener) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.imagePaths = imagePaths;
        this.onImageListener = onImageListener;
        this.imageDimension = (int) (helper.getDisplayHeight(context) * 0.18);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.profile_recycler_grid_view_item, parent, false);

        return new ViewHolder(view, onImageListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(context).load(imagePaths.get(position)).apply(new RequestOptions().override(imageDimension, imageDimension)).into(holder.profileImageItem);
    }

    @Override
    public int getItemCount() {
        return imagePaths.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView profileImageItem;
        OnImageListener onImageListener;

        ViewHolder(@NonNull View itemView, OnImageListener onImageListener) {
            super(itemView);
            profileImageItem = itemView.findViewById(R.id.profile_image_item);
            this.onImageListener = onImageListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onImageListener.onImageListener(getAdapterPosition());
        }
    }

    public interface OnImageListener {
        void onImageListener(int position);
    }
}
