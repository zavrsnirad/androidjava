package com.example.android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.android.Activity.AuthenticatedActivity;
import com.example.android.Common.Helper;
import com.example.android.DataModel.UsersInSearch;
import com.example.android.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SearchRecyclerListAdapter extends RecyclerView.Adapter<SearchRecyclerListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<UsersInSearch> usersInSearch;
    private AuthenticatedActivity activity;
    private LayoutInflater layoutInflater;
    private Helper helper = new Helper();
    private int imageDimension;

    public SearchRecyclerListAdapter(Context context, ArrayList<UsersInSearch> usersInSearch, AuthenticatedActivity activity) {
        this.context = context;
        this.usersInSearch = usersInSearch;
        this.activity = activity;
        this.layoutInflater = LayoutInflater.from(context);
        this.imageDimension = (int) (helper.getDisplayHeight(context) * 0.1);
    }

    @NonNull
    @Override
    public SearchRecyclerListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.search_recycler_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        if (usersInSearch.get(position).getProfileImage() != null) {
            Glide.with(context).load(usersInSearch.get(position).getProfileImage()).apply(new RequestOptions().override(imageDimension, imageDimension)).into(holder.searchProfileImage);
        } else {
            Glide.with(context).load(R.mipmap.ic_profile_circle).apply(new RequestOptions().override(imageDimension, imageDimension)).into(holder.searchProfileImage);
        }
        holder.searchUsername.setText(usersInSearch.get(position).getUsername());

        holder.searchUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.backList.add(R.id.nav_home);
                helper.openProfileFragment(context, usersInSearch.get(position).getUsername(), usersInSearch.get(position).getUserId());
            }
        });
        holder.searchProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.backList.add(R.id.nav_home);
                helper.openProfileFragment(context, usersInSearch.get(position).getUsername(), usersInSearch.get(position).getUserId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersInSearch.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView searchProfileImage;
        TextView searchUsername;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            searchProfileImage = itemView.findViewById(R.id.search_profile_picture);
            searchUsername = itemView.findViewById(R.id.search_username);
        }
    }
}
