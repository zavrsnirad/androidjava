package com.example.android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.android.Activity.AuthenticatedActivity;
import com.example.android.Common.Helper;
import com.example.android.DataModel.ImagesOnHome;
import com.example.android.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeRecyclerListAdapter extends RecyclerView.Adapter<HomeRecyclerListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ImagesOnHome> imagesOnHome;
    private LayoutInflater layoutInflater;
    private AuthenticatedActivity activity;
    private Helper helper = new Helper();
    private int imageDimension;
    private int profileDimension;

    public HomeRecyclerListAdapter(Context context, ArrayList<ImagesOnHome> imagesOnHome, AuthenticatedActivity activity) {
        this.context = context;
        this.imagesOnHome = imagesOnHome;
        this.activity = activity;
        this.layoutInflater = LayoutInflater.from(context);
        this.imageDimension = (int) (helper.getDisplayHeight(context) * 0.55);
        this.profileDimension = (int) (helper.getDisplayHeight(context) * 0.05);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.home_recycler_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.homeUsername.setText(imagesOnHome.get(position).getUsername());
        Glide.with(context).load(imagesOnHome.get(position).getImage()).apply(new RequestOptions().override(imageDimension, imageDimension)).into(holder.homeImage);
        if (imagesOnHome.get(position).getProfileImage() != null) {
            Glide.with(context).load(imagesOnHome.get(position).getProfileImage()).apply(new RequestOptions().override(profileDimension, profileDimension)).into(holder.homeProfileImage);
        } else {
            Glide.with(context).load(R.mipmap.ic_profile_circle).apply(new RequestOptions().override(profileDimension, profileDimension)).into(holder.homeProfileImage);
        }
        if (imagesOnHome.get(position).getDescription() != null) {
            holder.homeImageDescription.setText(imagesOnHome.get(position).getDescription());
        }
        holder.homeUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.backList.add(R.id.nav_home);
                helper.openProfileFragment(context, imagesOnHome.get(position).getUsername(), imagesOnHome.get(position).getUserId());
            }
        });
        holder.homeProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.backList.add(R.id.nav_home);
                helper.openProfileFragment(context, imagesOnHome.get(position).getUsername(), imagesOnHome.get(position).getUserId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesOnHome.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView homeProfileImage;
        TextView homeUsername;
        ImageView homeImage;
        TextView homeImageDescription;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            homeProfileImage = itemView.findViewById(R.id.home_profile_image);
            homeUsername = itemView.findViewById(R.id.home_username);
            homeImage = itemView.findViewById(R.id.home_image);
            homeImageDescription = itemView.findViewById(R.id.home_image_description);
        }
    }
}
