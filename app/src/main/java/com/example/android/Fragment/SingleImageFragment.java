package com.example.android.Fragment;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.android.API.RetrofitApi;
import com.example.android.R;
import com.example.android.Util.SharedPref;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Objects;

@EFragment(R.layout.single_image_fragment)
class SingleImageFragment extends Fragment {

    @ViewById
    ImageView singleImage;
    @ViewById
    TextView singleImageUsername;
    @ViewById
    TextView singleImageDescription;

    private SharedPref sharedPref = new SharedPref();

    @AfterViews
    void init() {
        singleImageUsername.setText(sharedPref.getUsername(getActivity()));
        if (getArguments() != null) {
            singleImageDescription.setText(getArguments().getString("description"));
            String imageFileName = getArguments().getString("imageFileName");
            String imagePath = RetrofitApi.WEB_URL + imageFileName;
            Glide.with(Objects.requireNonNull(getActivity())).load(imagePath).into(singleImage);
        }
    }
}
