package com.example.android.Fragment;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.android.API.Model.RestDayBody;
import com.example.android.API.Request.RestDayInterface;
import com.example.android.API.Response.AllRestDays;
import com.example.android.API.Response.RestDay;
import com.example.android.API.RetrofitApi;
import com.example.android.Adapter.RestDayRecyclerListAdapter;
import com.example.android.Common.Helper;
import com.example.android.R;
import com.example.android.Util.SharedPref;
import com.google.android.material.navigation.NavigationView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.android.Common.ResponseTypes.ERROR_ACTIVE_REST_DAY;
import static com.example.android.Common.ResponseTypes.ERROR_REST_DAYS;
import static com.example.android.Common.ResponseTypes.SUCCESS_ACTIVE_REST_DAY;
import static com.example.android.Common.ResponseTypes.SUCCESS_REST_DAY;
import static com.example.android.Common.ResponseTypes.SUCCESS_REST_DAYS;

@EFragment(R.layout.rest_day_fragment)
class RestDayFragment extends Fragment {

    @ViewById
    Spinner daySpinner;
    @ViewById
    Button setRestDay;
    @ViewById
    TextView activeRestDay;
    @ViewById
    TextView setRestDayText;
    @ViewById
    ConstraintLayout restDayLayout;
    @ViewById
    ProgressBar progressBarRestDay;
    @ViewById
    RecyclerView restDayList;
    @ViewById
    NestedScrollView scrollRestDayList;
    @ViewById
    NavigationView restDaysNv;
    @ViewById
    SwipeRefreshLayout swipeLayout;
    @ViewById
    DrawerLayout restDayDrawer;

    private String restDay;
    private SharedPref sharedPref = new SharedPref();
    private RestDayInterface restDayApiClient = RetrofitApi.getRestDayApi();
    private Helper helper = new Helper();
    private ArrayList<String> dayList;
    private ArrayList<String> activeFromList;
    private ArrayList<String> activeToList;
    private Call<RestDay> restDayCall;
    private Call<AllRestDays> allRestDaysCall;
    private String activeRestDayText = null;
    private String customText = null;
    private boolean isActiveRestDayRequestDone = false;
    private boolean isAllRestDaysRequestDone = false;

    @AfterViews
    void init() {
        initDrawerListener();
        swipeLayout.setDistanceToTriggerSync(120);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initRestDayFragment(false);
            }
        });
        boolean areRequestsDone = isActiveRestDayRequestDone && isAllRestDaysRequestDone;
        if (getArguments() != null && areRequestsDone) {
            if (getArguments().getBoolean("fromBackStack")) {
                activeRestDay.setText(activeRestDayText);
                setRestDayText.setText(customText);
                restDayList.setLayoutManager(new LinearLayoutManager(getContext()));
                RestDayRecyclerListAdapter restDayRecyclerListAdapter = new RestDayRecyclerListAdapter(getContext(), dayList, activeFromList, activeToList);
                restDayList.setAdapter(restDayRecyclerListAdapter);
                initDaySpinner();
                restDaysNv.setVisibility(View.VISIBLE);
                restDayLayout.setVisibility(View.VISIBLE);
            }

        } else {
            initRestDayFragment(true);
        }
    }

    @Click
    void setRestDay() {
        createRestDay(restDay);
    }

    @Click
    void triggerDrawer() {
        restDayDrawer.openDrawer(GravityCompat.START);
    }

    private void initRestDayFragment(boolean showLoader) {
        restDayLayout.setVisibility(View.GONE);
        if (showLoader) {
            progressBarRestDay.setVisibility(View.VISIBLE);
        }
        setRestDay.setEnabled(false);
        restDayCall = restDayApiClient.getActiveRestDay(sharedPref.getUserToken(getContext()));
        allRestDaysCall = restDayApiClient.getAllRestDays(sharedPref.getUserToken(getContext()));
        dayList = new ArrayList<>();
        activeFromList = new ArrayList<>();
        activeToList = new ArrayList<>();
        initDaySpinner();
        getActiveRestDay(restDayCall);
        getAllRestDays(allRestDaysCall);
    }

    private void initDaySpinner() {
        String[] days = new String[]{
                "Select a day",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday"
        };

        ArrayList<String> daysList = new ArrayList<>(Arrays.asList(days));
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getContext(), R.layout.day_spinner_item, daysList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(Color.GRAY);
                } else {
                    textView.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerAdapter.setDropDownViewResource(R.layout.day_spinner_item);
        daySpinner.setAdapter(spinnerAdapter);
        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (adapterView.getSelectedItem().toString()) {
                    case "Monday":
                        restDay = "Mon";
                        setRestDay.setEnabled(true);
                        break;
                    case "Tuesday":
                        restDay = "Tue";
                        setRestDay.setEnabled(true);
                        break;
                    case "Wednesday":
                        restDay = "Wed";
                        setRestDay.setEnabled(true);
                        break;
                    case "Thursday":
                        restDay = "Thu";
                        setRestDay.setEnabled(true);
                        break;
                    case "Friday":
                        restDay = "Fri";
                        setRestDay.setEnabled(true);
                        break;
                    case "Saturday":
                        restDay = "Sat";
                        setRestDay.setEnabled(true);
                        break;
                    case "Sunday":
                        restDay = "Sun";
                        setRestDay.setEnabled(true);
                        break;
                    default:
                        restDay = null;
                        setRestDay.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initDrawerListener() {
        restDayDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                swipeLayout.setRefreshing(false);
                swipeLayout.setEnabled(false);
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                swipeLayout.setEnabled(true);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    private void getActiveRestDay(Call<RestDay> call) {
        call.enqueue(new Callback<RestDay>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<RestDay> call, Response<RestDay> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_ACTIVE_REST_DAY:
                                activeRestDayText = "Your current rest day is " + response.body().getRestDayData().getRestDay();
                                customText = getString(R.string.set_new_rest_day_text);
                                activeRestDay.setText(activeRestDayText);
                                setRestDayText.setText(customText);
                                break;
                            case ERROR_ACTIVE_REST_DAY:
                                activeRestDayText = getString(R.string.no_active_rest_day);
                                customText = getString(R.string.set_rest_day_text);
                                activeRestDay.setText(activeRestDayText);
                                setRestDayText.setText(customText);
                                break;
                        }
                        isActiveRestDayRequestDone = true;
                    }
                }
            }

            @Override
            public void onFailure(Call<RestDay> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }

    private void getAllRestDays(Call<AllRestDays> call) {
        call.enqueue(new Callback<AllRestDays>() {
            @Override
            public void onResponse(Call<AllRestDays> call, Response<AllRestDays> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_REST_DAYS:
                                dayList.clear();
                                activeFromList.clear();
                                activeToList.clear();
                                for (int i = 0; i < response.body().getRestDayData().size(); i++) {
                                    dayList.add(response.body().getRestDayData().get(i).getRestDay());
                                    activeFromList.add(response.body().getRestDayData().get(i).getActiveFrom().getFormattedDate());
                                    if (response.body().getRestDayData().get(i).getActiveTo() == null) {
                                        activeToList.add("Till the end of time");
                                    } else {
                                        activeToList.add(response.body().getRestDayData().get(i).getActiveTo().getFormattedDate());
                                    }
                                }

                                restDayList.setLayoutManager(new LinearLayoutManager(getContext()));
                                RestDayRecyclerListAdapter restDayRecyclerListAdapter = new RestDayRecyclerListAdapter(getContext(), dayList, activeFromList, activeToList);
                                restDayList.setAdapter(restDayRecyclerListAdapter);
                                break;
                            case ERROR_REST_DAYS:

                                break;
                        }
                        isAllRestDaysRequestDone = true;
                        restDayLayout.setVisibility(View.VISIBLE);
                        progressBarRestDay.setVisibility(View.GONE);
                        restDaysNv.setVisibility(View.VISIBLE);
                        swipeLayout.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<AllRestDays> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }

    private void createRestDay(String restDay) {
        RestDayBody restDayBody = new RestDayBody(restDay);
        restDayApiClient.createRestDay(sharedPref.getUserToken(getContext()), restDayBody).enqueue(new Callback<RestDay>() {
            @Override
            public void onResponse(Call<RestDay> call, Response<RestDay> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (SUCCESS_REST_DAY.equals(response.body().getType())) {
                            String activeFrom = response.body().getRestDayData().getActiveFrom().getFormattedDate();
                            String text = "Rest day created successfully and it will be active from " + activeFrom;
                            helper.showAlert(getContext(), null, text, R.drawable.ic_rest_day);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RestDay> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        restDayCall.cancel();
        allRestDaysCall.cancel();
    }
}
