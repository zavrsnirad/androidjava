package com.example.android.Fragment;

import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.example.android.API.ResReq.UserFactory;
import com.example.android.Activity.AuthenticationActivity;
import com.example.android.Common.UserCommon;
import com.example.android.R;
import com.google.android.material.textfield.TextInputEditText;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.login_fragment)
class LoginFragment extends Fragment {

    @ViewById
    TextInputEditText loginUsername;
    @ViewById
    TextInputEditText loginPassword;
    @ViewById
    Button loginButton;

    private UserFactory userFactory;
    private UserCommon userCommon;

    private boolean validUsername = false;
    private boolean validPassword = false;

    @AfterViews
    void init() {
        userFactory = new UserFactory();
        userCommon = new UserCommon();
        loginButton.setEnabled(false);
    }

    @Click(R.id.login_button)
    void loginButton() {
        userFactory.loginUser(
                (AuthenticationActivity) getActivity(),
                userCommon.getUsername(loginUsername),
                userCommon.getPassword(loginPassword)
        );
    }

    @AfterTextChange(R.id.login_username)
    void afterUsernameChange() {
        if (!userCommon.getUsername(loginUsername).isEmpty()
                && userCommon.isUsernameValid(userCommon.getUsername(loginUsername))) {
            validUsername = true;
            loginUsername.setError(null);
        } else {
            validUsername = false;
            loginUsername.setError(getString(R.string.username_error_message));
        }
    }

    @AfterTextChange(R.id.login_password)
    void afterPasswordChange() {
        if (!userCommon.getPassword(loginPassword).isEmpty()
                && userCommon.isPasswordValid(userCommon.getPassword(loginPassword))) {
            validPassword = true;
            loginPassword.setError(null);
        } else {
            validPassword = false;
            loginPassword.setError(getString(R.string.password_error_message));
        }
    }

    @AfterTextChange({R.id.login_username, R.id.login_password})
    void afterAllInputsChange() {
        if (validUsername && validPassword) {
            loginButton.setEnabled(true);
        } else {
            loginButton.setEnabled(false);
        }
    }
}
