package com.example.android.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.android.API.Model.FollowBody;
import com.example.android.API.Request.ImageInterface;
import com.example.android.API.Request.UserInterface;
import com.example.android.API.Response.Delete;
import com.example.android.API.Response.Image;
import com.example.android.API.Response.Mail;
import com.example.android.API.Response.UserFollowing;
import com.example.android.API.Response.UserImages;
import com.example.android.API.RetrofitApi;
import com.example.android.Activity.AuthenticatedActivity;
import com.example.android.Activity.AuthenticationActivity_;
import com.example.android.Adapter.ProfileRecyclerGridViewAdapter;
import com.example.android.Adapter.ProfileRecyclerListViewAdapter;
import com.example.android.Common.Helper;
import com.example.android.R;
import com.example.android.Util.SharedPref;
import com.google.android.material.navigation.NavigationView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.example.android.Common.ResponseTypes.ERROR_GET_USER_IMAGES;
import static com.example.android.Common.ResponseTypes.ERROR_IMAGE_UPLOAD;
import static com.example.android.Common.ResponseTypes.ERROR_USER_DATA;
import static com.example.android.Common.ResponseTypes.ERROR_USER_FOLLOWED;
import static com.example.android.Common.ResponseTypes.ERROR_USER_FOLLOWERS;
import static com.example.android.Common.ResponseTypes.ERROR_USER_FOLLOWING;
import static com.example.android.Common.ResponseTypes.ERROR_USER_PROFILE_IMAGE;
import static com.example.android.Common.ResponseTypes.ERROR_USER_UNFOLLOWED;
import static com.example.android.Common.ResponseTypes.MAIL_SENT;
import static com.example.android.Common.ResponseTypes.SUCCESS_GET_USER_IMAGES;
import static com.example.android.Common.ResponseTypes.SUCCESS_IMAGE_UPLOAD;
import static com.example.android.Common.ResponseTypes.SUCCESS_USER_FOLLOWED;
import static com.example.android.Common.ResponseTypes.SUCCESS_USER_FOLLOWERS;
import static com.example.android.Common.ResponseTypes.SUCCESS_USER_FOLLOWING;
import static com.example.android.Common.ResponseTypes.SUCCESS_USER_PROFILE_IMAGE;
import static com.example.android.Common.ResponseTypes.SUCCESS_USER_UNFOLLOWED;

@EFragment(R.layout.profile_fragment)
class ProfileFragment extends Fragment {

    @ViewById
    RecyclerView profileRecyclerView;
    @ViewById
    ImageView profileImage;
    @ViewById
    TextView profileUsername;
    @ViewById
    LinearLayout noImagesScreen;
    @ViewById
    RelativeLayout profileLayout;
    @ViewById
    ProgressBar progressBarProfile;
    @ViewById
    Button followUser;
    @ViewById
    SwipeRefreshLayout swipeLayout;
    @ViewById
    DrawerLayout profileDrawer;
    @ViewById
    NavigationView profileNv;
    @ViewById
    Switch dailyReminder;
    @ViewById
    TextView drawerFollowing;
    @ViewById
    TextView drawerFollowers;
    @ViewById
    Button triggerDrawer;
    @ViewById
    Button sendMail;

    private SharedPref sharedPref = new SharedPref();
    private ImageInterface imageApiClient = RetrofitApi.getImageApi();
    private UserInterface userApiClient = RetrofitApi.getUserApi();
    private Helper helper = new Helper();
    private ArrayList<String> imagePaths = new ArrayList<>();
    private ArrayList<String> deletePaths = new ArrayList<>();
    private ArrayList<String> descriptionList = new ArrayList<>();
    private ArrayList<String> followingUsernameList = new ArrayList<>();
    private ArrayList<Integer> followingIdList = new ArrayList<>();
    private ArrayList<String> followersUsernameList = new ArrayList<>();
    private ArrayList<Integer> followersIdList = new ArrayList<>();

    private String username;
    private String profileImageFileName;
    private ProfileRecyclerGridViewAdapter.OnImageListener onImageListener;
    private Call<UserImages> userImagesCall;
    private Call<UserFollowing> userFollowingCall;
    private Call<UserFollowing> userFollowersCall;
    private Call<UserFollowing> userFollowCall;
    private Call<UserFollowing> userUnfollowCall;
    private Call<UserImages> userProfileDataCall;
    private Call<Delete> deleteCall;
    private Call<Mail> mailCall;
    private int profileImageDimension = 0;
    private boolean followed;
    private boolean isProfileImageRequestDone = false;
    private boolean isImagesRequestDone = false;
    private boolean isFollowingRequestDone = false;
    private boolean isFollowersRequestDone = false;
    private boolean isMyProfile = true;

    @SuppressLint("SetTextI18n")
    @AfterViews
    void init() {
        boolean areRequestsDone = isProfileImageRequestDone && isImagesRequestDone && isFollowingRequestDone && isFollowersRequestDone;
        String requestUsername = sharedPref.getUsername(getContext());
        profileImageDimension = helper.getDisplayHeight(getContext()) / 8;

        if (getArguments() != null) {
            if (getArguments().getString("username") != null) {
                isMyProfile = false;
                progressBarProfile.setVisibility(View.VISIBLE);
                followUser.setVisibility(View.VISIBLE);
                sendMail.setVisibility(View.VISIBLE);
                if (sharedPref.isMailSentToday(getContext())) {
                    sendMail.setEnabled(false);
                }
                requestUsername = getArguments().getString("username");
                initProfileFragment(requestUsername, true);
            } else if (getArguments().getBoolean("fromBackStack") && areRequestsDone) {
                profileUsername.setText(username);
                if (profileImageFileName != null) {
                    Glide.with(Objects.requireNonNull(getContext())).load(profileImageFileName).apply(new RequestOptions().override(profileImageDimension, profileImageDimension)).into(profileImage);
                } else {
                    Glide.with(Objects.requireNonNull(getContext())).load(R.mipmap.ic_profile_circle).apply(new RequestOptions().override(profileImageDimension, profileImageDimension)).into(profileImage);
                }
                if (imagePaths.isEmpty()) {
                    noImagesScreen.setVisibility(View.VISIBLE);
                } else {
                    profileRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
                    ProfileRecyclerGridViewAdapter profileRecyclerGridViewAdapter = new ProfileRecyclerGridViewAdapter(getContext(), imagePaths, onImageListener);
                    profileRecyclerView.setAdapter(profileRecyclerGridViewAdapter);
                }
                drawerFollowers.setText("Followers: " + followersIdList.size());
                drawerFollowing.setText("Followers: " + followingIdList.size());
                profileLayout.setVisibility(View.VISIBLE);
            } else {
                initProfileFragment(requestUsername, true);
            }
        } else {
            initProfileFragment(requestUsername, true);
        }

        if (followUser.getVisibility() == View.GONE) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) profileUsername.getLayoutParams();
            params.addRule(RelativeLayout.ABOVE, profileRecyclerView.getId());
            profileUsername.setLayoutParams(params);
        } else {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) profileUsername.getLayoutParams();
            params.addRule(RelativeLayout.ABOVE, followUser.getId());
            profileUsername.setLayoutParams(params);
        }

        if (isMyProfile) {
            triggerDrawer.setVisibility(View.VISIBLE);
        }

        if (triggerDrawer.getVisibility() == View.GONE) {
            RelativeLayout.LayoutParams profileImageParams = (RelativeLayout.LayoutParams) profileImage.getLayoutParams();
            RelativeLayout.LayoutParams usernameParams = (RelativeLayout.LayoutParams) profileUsername.getLayoutParams();
            usernameParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            profileImageParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            profileImage.setLayoutParams(profileImageParams);
            profileUsername.setLayoutParams(usernameParams);
        }

        initDrawerListener();
        swipeLayout.setDistanceToTriggerSync(120);
        final String username = requestUsername;
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initProfileFragment(username, false);
            }
        });

        final String finalRequestUsername = requestUsername;
        onImageListener = new ProfileRecyclerGridViewAdapter.OnImageListener() {
            @Override
            public void onImageListener(int position) {
                profileRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                ProfileRecyclerListViewAdapter profileRecyclerListViewAdapter = new ProfileRecyclerListViewAdapter(
                        getContext(),
                        imagePaths,
                        deletePaths,
                        descriptionList,
                        profileImageFileName,
                        finalRequestUsername,
                        isMyProfile
                );
                profileRecyclerView.setAdapter(profileRecyclerListViewAdapter);
            }
        };
    }

    @Click
    void dailyReminder() {
        if (sharedPref.isEnabledDailyNotification(getContext())) {
            sharedPref.setEnabledDailyNotification(getContext(), false);
        } else {
            sharedPref.setEnabledDailyNotification(getContext(), true);
        }
    }

    @Click
    void triggerDrawer() {
        profileDrawer.openDrawer(GravityCompat.START);
    }

    @Click
    void profileImage() {
        if (isMyProfile) {
            CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(1, 1).start(Objects.requireNonNull(getContext()), this);
        }
    }

    @Click
    void sendMail() {
        if (getArguments() != null) {
            mailCall = userApiClient.sendNotice(sharedPref.getUserToken(getContext()), getArguments().getInt("userId"));
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setCancelable(false)
                    .setIcon(R.mipmap.ic_mail)
                    .setTitle(R.string.send_email_title)
                    .setMessage(R.string.send_email_message)
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            sendNotice(mailCall);
                            sharedPref.setSentMail(getContext());
                            sendMail.setEnabled(false);
                        }
                    })
                    .setNeutralButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    @Click
    void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false)
                .setIcon(R.mipmap.ic_logout)
                .setTitle("Are you sure?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sharedPref.logoutUser(getContext());
                        AuthenticatedActivity activity = (AuthenticatedActivity) getActivity();
                        activity.finish();
                        AuthenticationActivity_.intent(getContext()).start();
                    }
                })
                .setNeutralButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Click
    void deleteUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false)
                .setIcon(R.mipmap.ic_delete_user)
                .setTitle("Delete profile")
                .setMessage(getString(R.string.delete_profile_message))
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteUser(deleteCall);
                    }
                })
                .setNeutralButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Click
    void followUser() {
        if (getArguments() != null) {
            if (followed) {
                userUnfollowCall = userApiClient.unfollowUser(sharedPref.getUserToken(getContext()), getArguments().getInt("userId"));
                userUnfollow(userUnfollowCall);
            } else {
                FollowBody followBody = new FollowBody(getArguments().getInt("userId"));
                userFollowCall = userApiClient.followUser(sharedPref.getUserToken(getContext()), followBody);
                userFollow(userFollowCall);
            }
        }
    }

    private void initProfileFragment(String requestUsername, boolean showLoader) {
        imagePaths.clear();
        profileImageFileName = null;
        deletePaths.clear();
        descriptionList.clear();
        followingUsernameList.clear();
        followingIdList.clear();
        followersUsernameList.clear();
        followersIdList.clear();
        profileLayout.setVisibility(View.GONE);
        noImagesScreen.setVisibility(View.GONE);
        if (showLoader) {
            progressBarProfile.setVisibility(View.VISIBLE);
        }
        if (!isMyProfile) {
            profileDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        if (sharedPref.isEnabledDailyNotification(getContext())) {
            dailyReminder.setChecked(true);
        } else {
            dailyReminder.setChecked(false);
        }
        userProfileDataCall = imageApiClient.getUserProfileImage(sharedPref.getUserToken(getContext()), requestUsername);
        userImagesCall = imageApiClient.getUserImages(sharedPref.getUserToken(getContext()), requestUsername);
        userFollowingCall = userApiClient.getFollowingUsers(sharedPref.getUserToken(getContext()), requestUsername);
        userFollowersCall = userApiClient.getFollowers(sharedPref.getUserToken(getContext()), requestUsername);
        deleteCall = userApiClient.deleteUser(sharedPref.getUserToken(getContext()));
        Glide.with(getContext()).load(R.mipmap.ic_profile_circle).apply(new RequestOptions().override(profileImageDimension, profileImageDimension)).into(profileImage);
        getUserProfileImage(getContext(), userProfileDataCall);
        getUserImages(userImagesCall);
        getUserFollowing(userFollowingCall);
        getUserFollowers(userFollowersCall);
    }

    private void initDrawerListener() {
        profileDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                swipeLayout.setRefreshing(false);
                swipeLayout.setEnabled(false);
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                swipeLayout.setEnabled(true);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    private void deleteUser(Call<Delete> call) {
        call.enqueue(new Callback<Delete>() {
            @Override
            public void onResponse(Call<Delete> call, Response<Delete> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        sharedPref.logoutUser(getContext());
                        AuthenticatedActivity activity = (AuthenticatedActivity) getActivity();
                        activity.finish();
                        AuthenticationActivity_.intent(getContext()).start();
                        Toast.makeText(getContext(), R.string.user_delete_message, Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Delete> call, Throwable t) {

            }
        });
    }

    private void getUserImages(Call<UserImages> call) {
        call.enqueue(new Callback<UserImages>() {
            @Override
            public void onResponse(Call<UserImages> call, Response<UserImages> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getUserData() != null) {
                        switch (response.body().getType()) {
                            case ERROR_USER_DATA:
                                helper.showAlert(getContext(), null, response.body().getMessage(), 0);
                            case SUCCESS_GET_USER_IMAGES:
                                for (int i = 0; i < response.body().getImageData().size(); i++) {
                                    imagePaths.add(RetrofitApi.WEB_URL + response.body().getImageData().get(i).getImageFileName());
                                    deletePaths.add(response.body().getImageData().get(i).getImageFileName());
                                    descriptionList.add(response.body().getImageData().get(i).getDescription());
                                }

                                if (imagePaths.isEmpty()) {
                                    noImagesScreen.setVisibility(View.VISIBLE);
                                } else {
                                    profileRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
                                    ProfileRecyclerGridViewAdapter profileRecyclerGridViewAdapter = new ProfileRecyclerGridViewAdapter(getContext(), imagePaths, onImageListener);
                                    profileRecyclerView.setAdapter(profileRecyclerGridViewAdapter);
                                }
                                profileRecyclerView.setVisibility(View.VISIBLE);
                                break;
                            case ERROR_GET_USER_IMAGES:
                                noImagesScreen.setVisibility(View.VISIBLE);
                                break;
                        }
                        isImagesRequestDone = true;
                    }
                }
            }

            @Override
            public void onFailure(Call<UserImages> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }

    private void getUserProfileImage(final Context context, Call<UserImages> call) {
        call.enqueue(new Callback<UserImages>() {
            @Override
            public void onResponse(Call<UserImages> call, Response<UserImages> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getUserData() != null) {
                        username = null;
                        profileImageFileName = null;
                        switch (response.body().getType()) {
                            case ERROR_USER_DATA:
                                helper.showAlert(getContext(), null, response.body().getMessage(), 0);
                            case SUCCESS_USER_PROFILE_IMAGE:
                                username = response.body().getUserData().get(0).getUsername();
                                profileUsername.setText(username);
                                profileImageFileName = RetrofitApi.WEB_URL + response.body().getImageData().get(0).getImageFileName();
                                Glide.with(context).load(RetrofitApi.WEB_URL + response.body().getImageData().get(0).getImageFileName()).apply(new RequestOptions().override(profileImageDimension, profileImageDimension)).into(profileImage);
                                break;
                            case ERROR_USER_PROFILE_IMAGE:
                                username = response.body().getUserData().get(0).getUsername();
                                profileUsername.setText(username);
                                profileImageFileName = null;
                                Glide.with(context).load(R.mipmap.ic_profile_circle).apply(new RequestOptions().override(profileImageDimension, profileImageDimension)).into(profileImage);
                                break;
                        }
                        isProfileImageRequestDone = true;
                    }
                }
            }

            @Override
            public void onFailure(Call<UserImages> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }

    private void uploadProfileImage(final Context context, MultipartBody.Part imageFile, RequestBody imageFilename) {
        String token = sharedPref.getUserToken(context);
        imageApiClient.uploadProfileImage(token, imageFile, imageFilename, 1, null).enqueue(new Callback<Image>() {
            @Override
            public void onResponse(Call<Image> call, Response<Image> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_IMAGE_UPLOAD:
                            case ERROR_IMAGE_UPLOAD:
                                helper.showAlert(context, null, response.body().getMessage(), R.drawable.ic_image);
                                break;
                        }

                    }
                } else {
                    helper.showAlert(context, null, getString(R.string.max_file_size), 0);
                }
            }

            @Override
            public void onFailure(Call<Image> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(context, null, t.getMessage(), 0);
                }
            }
        });
    }

    private void getUserFollowing(Call<UserFollowing> call) {
        call.enqueue(new Callback<UserFollowing>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<UserFollowing> call, Response<UserFollowing> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_USER_FOLLOWING:
                                followingIdList.clear();
                                followingUsernameList.clear();
                                for (int i = 0; i < response.body().getFollowingData().size(); i++) {
                                    followingIdList.add(response.body().getFollowingData().get(i).getUserFollowed());
                                    followingUsernameList.add(response.body().getFollowingData().get(i).getUserFollowing());
                                }
                                drawerFollowing.setText("Following: " + followingIdList.size());
                                break;
                            case ERROR_USER_FOLLOWING:
                                drawerFollowing.setText(R.string.FollowingZero);
                                break;
                        }
                        isFollowingRequestDone = true;
                    }
                }
            }

            @Override
            public void onFailure(Call<UserFollowing> call, Throwable t) {

            }
        });
    }

    private void getUserFollowers(Call<UserFollowing> call) {
        call.enqueue(new Callback<UserFollowing>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<UserFollowing> call, Response<UserFollowing> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_USER_FOLLOWERS:
                                followersIdList.clear();
                                followersUsernameList.clear();
                                for (int i = 0; i < response.body().getFollowingData().size(); i++) {
                                    followersIdList.add(response.body().getFollowingData().get(i).getUserFollowed());
                                    followersUsernameList.add(response.body().getFollowingData().get(i).getUserFollowing());
                                }
                                followed = followersUsernameList.contains(sharedPref.getUsername(getContext()));
                                if (followed) {
                                    followUser.setText(getString(R.string.unfollow));
                                }
                                drawerFollowers.setText("Followers: " + followersIdList.size());
                                break;
                            case ERROR_USER_FOLLOWERS:
                                drawerFollowers.setText(R.string.FollowersZero);
                                break;
                        }
                        profileLayout.setVisibility(View.VISIBLE);
                        progressBarProfile.setVisibility(View.GONE);
                        isFollowersRequestDone = true;
                        swipeLayout.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<UserFollowing> call, Throwable t) {

            }
        });
    }

    private void userFollow(Call<UserFollowing> call) {
        call.enqueue(new Callback<UserFollowing>() {
            @Override
            public void onResponse(Call<UserFollowing> call, Response<UserFollowing> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_USER_FOLLOWED:
                                followed = true;
                                followUser.setText(getString(R.string.unfollow));
                                helper.showAlert(getContext(), null, response.body().getMessage(), 0);
                                break;
                            case ERROR_USER_FOLLOWED:
                                helper.showAlert(getContext(), null, response.body().getMessage(), 0);
                                break;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UserFollowing> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }

    private void userUnfollow(Call<UserFollowing> call) {
        call.enqueue(new Callback<UserFollowing>() {
            @Override
            public void onResponse(Call<UserFollowing> call, Response<UserFollowing> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_USER_UNFOLLOWED:
                                followed = false;
                                followUser.setText(getString(R.string.follow));
                                helper.showAlert(getContext(), null, response.body().getMessage(), 0);
                                break;
                            case ERROR_USER_UNFOLLOWED:
                                helper.showAlert(getContext(), null, response.body().getMessage(), 0);
                                break;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UserFollowing> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }

    private void sendNotice(Call<Mail> call) {
        call.enqueue(new Callback<Mail>() {
            @Override
            public void onResponse(Call<Mail> call, Response<Mail> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getType().equals(MAIL_SENT)) {
                            helper.showAlert(getContext(), null, response.body().getMessage(), 0);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Mail> call, Throwable t) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri imageUri = result.getUri();
                File originalFile = new File(result.getUri().getPath());
                RequestBody imageFileName = RequestBody.create(
                        MultipartBody.FORM, "profileImage"
                );
                RequestBody filePart = RequestBody.create(
                        MediaType.parse("image"),
                        originalFile
                );
                MultipartBody.Part imageFile = MultipartBody.Part.createFormData("imageFile", originalFile.getName(), filePart);
                Glide.with(Objects.requireNonNull(getContext())).load(imageUri).into(profileImage);
                uploadProfileImage(getActivity(), imageFile, imageFileName);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        userProfileDataCall.cancel();
        userImagesCall.cancel();
        userFollowingCall.cancel();
        userFollowersCall.cancel();
        if (userFollowCall != null) {
            userFollowCall.cancel();
        }
        if (userUnfollowCall != null) {
            userUnfollowCall.cancel();
        }
        deleteCall.cancel();
        if (mailCall != null) {
            mailCall.cancel();
        }
    }
}
