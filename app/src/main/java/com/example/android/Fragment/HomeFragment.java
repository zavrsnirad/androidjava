package com.example.android.Fragment;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.android.API.Request.UserInterface;
import com.example.android.API.Response.HomeImage;
import com.example.android.API.Response.UserSearch;
import com.example.android.API.RetrofitApi;
import com.example.android.Activity.AuthenticatedActivity;
import com.example.android.Adapter.HomeRecyclerListAdapter;
import com.example.android.Adapter.SearchRecyclerListAdapter;
import com.example.android.Common.Helper;
import com.example.android.DataModel.ImagesOnHome;
import com.example.android.DataModel.UsersInSearch;
import com.example.android.R;
import com.example.android.Util.SharedPref;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.android.Common.ResponseTypes.ERROR_HOME_IMAGES;
import static com.example.android.Common.ResponseTypes.ERROR_USER_FOLLOWING;
import static com.example.android.Common.ResponseTypes.ERROR_USER_SEARCH;
import static com.example.android.Common.ResponseTypes.SUCCESS_HOME_IMAGES;
import static com.example.android.Common.ResponseTypes.SUCCESS_USER_SEARCH;

@EFragment(R.layout.home_fragment)
class HomeFragment extends Fragment {

    private Context context;

    @ViewById
    SearchView searchUsers;
    @ViewById
    RecyclerView searchResults;
    @ViewById
    RecyclerView homeImages;
    @ViewById
    ProgressBar progressBarHome;
    @ViewById
    RelativeLayout homeLayout;
    @ViewById
    SwipeRefreshLayout swipeLayout;

    private ArrayList<UsersInSearch> usersInSearch = new ArrayList<>();
    private ArrayList<ImagesOnHome> imagesOnHome = new ArrayList<>();
    private SharedPref sharedPref = new SharedPref();
    private UserInterface userApiClient = RetrofitApi.getUserApi();
    private Helper helper = new Helper();
    private Call<UserSearch> userSearchCall;
    private Call<HomeImage> homeImageCall;
    private boolean isHomeRequestDone = false;

    @AfterViews
    void init() {
        context = getContext();
        swipeLayout.setDistanceToTriggerSync(120);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initHomeFragment(false);
            }
        });
        if (getArguments() != null && isHomeRequestDone) {
            if (getArguments().getBoolean("fromBackStack")) {
                homeLayout.setVisibility(View.VISIBLE);
                homeImages.setLayoutManager(new LinearLayoutManager(context));
                HomeRecyclerListAdapter homeRecyclerListAdapter = new HomeRecyclerListAdapter(
                        context,
                        imagesOnHome,
                        (AuthenticatedActivity) getActivity()
                );
                homeImages.setAdapter(homeRecyclerListAdapter);
            }
        } else {
            initHomeFragment(true);
        }

        searchUsers.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchTerms) {
                if (searchTerms.length() >= 3) {
                    searchUsers(sharedPref.getUserToken(context), searchTerms);
                    homeImages.setVisibility(View.GONE);
                    searchResults.setVisibility(View.VISIBLE);
                    return true;
                } else {
                    usersInSearch.clear();
                    searchResults.setVisibility(View.GONE);
                    homeImages.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });
        searchUsers.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                searchResults.setVisibility(View.GONE);
                return false;
            }
        });
        searchResults.setLayoutManager(new LinearLayoutManager(context));
    }

    private void initHomeFragment(boolean showLoader) {
        homeLayout.setVisibility(View.GONE);
        if (showLoader) {
            progressBarHome.setVisibility(View.VISIBLE);
        }
        homeImageCall = userApiClient.getFollowingUsersImages(sharedPref.getUserToken(context));
        getFollowingUsersImages(homeImageCall);
    }

    private void getFollowingUsersImages(Call<HomeImage> call) {
        call.enqueue(new Callback<HomeImage>() {
            @Override
            public void onResponse(Call<HomeImage> call, Response<HomeImage> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case ERROR_USER_FOLLOWING:
                            case ERROR_HOME_IMAGES:
                                break;
                            case SUCCESS_HOME_IMAGES:
                                imagesOnHome.clear();
                                for (int i = 0; i < response.body().getHomeImageData().size(); i++) {
                                    Integer userId = response.body().getHomeImageData().get(i).getId();
                                    String username = response.body().getHomeImageData().get(i).getUsername();
                                    String profileImage = response.body().getHomeImageData().get(i).getProfileImageFileName() != null ?
                                            RetrofitApi.WEB_URL + response.body().getHomeImageData().get(i).getProfileImageFileName() :
                                            null;
                                    String image = RetrofitApi.WEB_URL + response.body().getHomeImageData().get(i).getImageFileName();
                                    String description = response.body().getHomeImageData().get(i).getDescription();
                                    imagesOnHome.add(new ImagesOnHome(
                                            userId,
                                            username,
                                            profileImage,
                                            image,
                                            description
                                    ));
                                }
                                homeImages.setLayoutManager(new LinearLayoutManager(context));
                                HomeRecyclerListAdapter homeRecyclerListAdapter = new HomeRecyclerListAdapter(
                                        context,
                                        imagesOnHome,
                                        (AuthenticatedActivity) getActivity()
                                );
                                homeImages.setAdapter(homeRecyclerListAdapter);
                                break;
                        }
                        progressBarHome.setVisibility(View.GONE);
                        homeLayout.setVisibility(View.VISIBLE);
                        isHomeRequestDone = true;
                        swipeLayout.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeImage> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(context, null, t.getMessage(), 0);
                }
            }
        });
    }

    private void searchUsers(String token, String searchTerms) {
        userSearchCall = userApiClient.searchUsers(token, searchTerms);
        userSearchCall.enqueue(new Callback<UserSearch>() {
            @Override
            public void onResponse(Call<UserSearch> call, Response<UserSearch> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_USER_SEARCH:
                                usersInSearch.clear();
                                for (int i = 0; i < response.body().getUserSearchData().size(); i++) {
                                    Integer userId = response.body().getUserSearchData().get(i).getId();
                                    String username = response.body().getUserSearchData().get(i).getUsername();
                                    String profileImage = response.body().getUserSearchData().get(i).getImageFileName() != null ?
                                            RetrofitApi.WEB_URL + response.body().getUserSearchData().get(i).getImageFileName() :
                                            null;
                                    usersInSearch.add(new UsersInSearch(
                                            userId,
                                            username,
                                            profileImage
                                    ));
                                }
                                SearchRecyclerListAdapter searchRecyclerListAdapter = new SearchRecyclerListAdapter(context, usersInSearch, (AuthenticatedActivity) getActivity());
                                if (searchResults != null) {
                                    searchResults.setAdapter(searchRecyclerListAdapter);
                                }
                                break;
                            case ERROR_USER_SEARCH:
                                break;
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<UserSearch> call, Throwable t) {
                if (!userSearchCall.isCanceled()) {
                    helper.showAlert(context, null, t.getMessage(), 0);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        homeImageCall.cancel();
        if (userSearchCall != null) {
            userSearchCall.cancel();
        }
    }
}
