package com.example.android.Fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.android.API.Request.ImageInterface;
import com.example.android.API.Response.Image;
import com.example.android.API.RetrofitApi;
import com.example.android.Common.Helper;
import com.example.android.R;
import com.example.android.Util.SharedPref;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.example.android.Common.ResponseTypes.ERROR_IMAGE_UPLOAD;
import static com.example.android.Common.ResponseTypes.SUCCESS_IMAGE_UPLOAD;

@EFragment(R.layout.image_upload_fragment)
class ImageUploadFragment extends Fragment {

    private static final int PICK_IMAGE = 1;
    private static final int PERMISSIONS_REQUEST_CODE = 2;

    @ViewById
    ImageView imageLoad;
    @ViewById
    Button uploadImage;
    @ViewById
    EditText imageDescription;

    private RequestBody imageFileName;
    private MultipartBody.Part imageFile;
    private SharedPref sharedPref = new SharedPref();
    private ImageInterface imageApiClient = RetrofitApi.getImageApi();
    private Helper helper = new Helper();

    private String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    @AfterViews
    void init() {
        uploadImage.setEnabled(false);
    }

    @Click(R.id.image_load)
    void pickImage() {
        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
        } else {
            openImagePicker();
        }
    }

    @Click(R.id.upload_image)
    void uploadImage() {
        RequestBody description = RequestBody.create(
                MultipartBody.FORM, imageDescription.getText().toString()
        );
        uploadUserImage(getActivity(), imageFile, imageFileName, description);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            openImagePicker();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri imageUri = data.getData();
            String imagePath = convertMediaUriToPath(Objects.requireNonNull(getActivity()), imageUri);
            File originalFile = new File(imagePath);

            imageFileName = RequestBody.create(
                    MultipartBody.FORM, getFilename(Objects.requireNonNull(getActivity()),
                            imageUri));
            RequestBody filePart = RequestBody.create(
                    MediaType.parse("image"),
                    originalFile);
            imageFile = MultipartBody.Part.createFormData("imageFile", originalFile.getName(), filePart);

            Glide.with(Objects.requireNonNull(getActivity())).load(imageUri).into(imageLoad);

            if (imageFile != null) {
                uploadImage.setEnabled(true);
            }
        }
    }

    private String getFilename(Context context, Uri contentUri) {
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        assert cursor != null;
        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        cursor.moveToFirst();
        String filename = cursor.getString(nameIndex);
        cursor.close();
        return filename;
    }

    private String convertMediaUriToPath(Context context, Uri uri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        assert cursor != null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }

    private void openImagePicker() {
        Intent intent = new Intent(
                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        );
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void uploadUserImage(final Context context, MultipartBody.Part imageFile, RequestBody imageFilename, RequestBody description) {
        String token = sharedPref.getUserToken(context);
        imageApiClient.uploadUserImage(token, imageFile, imageFilename, 0, description).enqueue(new Callback<Image>() {
            @Override
            public void onResponse(Call<Image> call, Response<Image> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_IMAGE_UPLOAD:
                                helper.showAlert(context, null, response.body().getMessage(), R.drawable.ic_image);
                                helper.openSingleImageFragment(
                                        context,
                                        response.body().getImageData().getImageFileName(),
                                        response.body().getImageData().getDescription()
                                );
                                break;
                            case ERROR_IMAGE_UPLOAD:
                                helper.showAlert(context, null, response.body().getMessage(), R.drawable.ic_image);
                                break;
                        }

                    }
                } else {
                    helper.showAlert(context, null, getString(R.string.max_file_size), 0);
                }
            }

            @Override
            public void onFailure(Call<Image> call, Throwable t) {
                helper.showAlert(context, null, t.getMessage(), 0);
            }
        });
    }
}
