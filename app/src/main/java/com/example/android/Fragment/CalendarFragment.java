package com.example.android.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.android.API.Model.WorkoutDayBody;
import com.example.android.API.Request.CalendarInterface;
import com.example.android.API.Request.RestDayInterface;
import com.example.android.API.Response.RestDay;
import com.example.android.API.Response.UserCalendar;
import com.example.android.API.Response.WorkoutDay;
import com.example.android.API.RetrofitApi;
import com.example.android.Activity.AuthenticatedActivity;
import com.example.android.Common.Helper;
import com.example.android.R;
import com.example.android.Util.SharedPref;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.android.Common.ResponseTypes.ERROR_ACTIVE_REST_DAY;
import static com.example.android.Common.ResponseTypes.ERROR_GET_USER_CALENDAR;
import static com.example.android.Common.ResponseTypes.ERROR_REST_DAY;
import static com.example.android.Common.ResponseTypes.ERROR_TODAY_WORKOUT;
import static com.example.android.Common.ResponseTypes.ERROR_USER_DATA;
import static com.example.android.Common.ResponseTypes.ERROR_WORKOUT_DAY;
import static com.example.android.Common.ResponseTypes.SUCCESS_ACTIVE_REST_DAY;
import static com.example.android.Common.ResponseTypes.SUCCESS_GET_USER_CALENDAR;
import static com.example.android.Common.ResponseTypes.SUCCESS_TODAY_WORKOUT;
import static com.example.android.Common.ResponseTypes.SUCCESS_WORKOUT_DAY;

@EFragment(R.layout.calendar_fragment)
class CalendarFragment extends Fragment {

    @ViewById
    CalendarView userCalendar;
    @ViewById
    Button saveWorkoutDay;
    @ViewById
    ConstraintLayout calendarLayout;
    @ViewById
    ProgressBar progressBarCalendar;
    @ViewById
    SwipeRefreshLayout swipeLayout;

    private SharedPref sharedPref = new SharedPref();
    private RestDayInterface restDayApiClient = RetrofitApi.getRestDayApi();
    private CalendarInterface calendarApiClient = RetrofitApi.getCalendarApi();
    private Helper helper = new Helper();
    private ArrayList<Long> timestampList = new ArrayList<>();
    private ArrayList<String> workoutDateList = new ArrayList<>();
    private ArrayList<Boolean> isWorkoutDoneList = new ArrayList<>();
    private Call<RestDay> restDayCall;
    private Call<WorkoutDay> workoutDayCall;
    private Call<UserCalendar> userCalendarCall;
    private AuthenticatedActivity activity;
    private boolean isRestDayRequestDone = false;
    private boolean isCalendarRequestDone = false;
    private boolean isWorkoutRequestDone = false;
    private boolean isButtonEnabled = false;
    private boolean hasRestDay = false;

    @AfterViews
    void init() {
        activity = (AuthenticatedActivity) getActivity();
        boolean areRequestsDone = isRestDayRequestDone && isCalendarRequestDone && isWorkoutRequestDone;
        final String requestUsername = sharedPref.getUsername(Objects.requireNonNull(getContext()));
        swipeLayout.setDistanceToTriggerSync(120);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initCalendarFragment(requestUsername, false);
            }
        });

        if (getArguments() != null && areRequestsDone && hasRestDay) {
            if (getArguments().getBoolean("fromBackStack")) {
                progressBarCalendar.setVisibility(View.GONE);
                calendarLayout.setVisibility(View.VISIBLE);
                userCalendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {
                        if (timestampList.contains(getClickedTimestamp(year, month, day))) {
                            int index = timestampList.indexOf(getClickedTimestamp(year, month, day));
                            showAlertForForWorkoutDate(workoutDateList.get(index), isWorkoutDoneList.get(index));
                        } else {
                            showAlertForForWorkoutDate(getClickedDate(year, month, day), null);
                        }
                    }
                });
                saveWorkoutDay.setEnabled(isButtonEnabled);
            }
        } else {
            initCalendarFragment(requestUsername, true);
        }
    }

    @Click
    void saveWorkoutDay() {
        showAlertWhenSavingWorkoutDay();
    }

    private void initCalendarFragment(String requestUsername, boolean showLoader) {
        calendarLayout.setVisibility(View.GONE);
        if (showLoader) {
            progressBarCalendar.setVisibility(View.VISIBLE);
        }
        if (getArguments() != null) {
            if (getArguments().getString("username") != null) {
                requestUsername = getArguments().getString("username");
            }
        }

        restDayCall = restDayApiClient.getActiveRestDay(sharedPref.getUserToken(getContext()));
        userCalendarCall = calendarApiClient.getUserCalendar(sharedPref.getUserToken(getContext()), requestUsername);
        workoutDayCall = calendarApiClient.getTodayWorkout(sharedPref.getUserToken(getContext()));

        checkActiveRestDay(restDayCall);
        getUserCalendar(userCalendarCall);
        checkTodayWorkout(workoutDayCall);
    }

    private void showAlertForNoRestDay() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false)
                .setIcon(R.drawable.ic_warning)
                .setTitle("No rest day")
                .setMessage(getString(R.string.dialog_rest_day_message))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        helper.openRestDayFragment(
                                activity.bottomAuthenticatedNavigation,
                                getContext(),
                                new RestDayFragment_()
                                , 4);
                    }
                });
        activity.backList.add(R.id.nav_calendar);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showAlertWhenSavingWorkoutDay() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false)
                .setIcon(R.drawable.ic_workout)
                .setTitle("Save workout day")
                .setMessage(getString(R.string.save_workout_day_dialog_text))
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        saveWorkoutDay(1);
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        saveWorkoutDay(0);
                    }
                })
                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showAlertForForWorkoutDate(String clickedDate, Boolean isWorkoutDone) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setCancelable(false)
                .setTitle(clickedDate)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        if (isWorkoutDone != null) {
            if (isWorkoutDone) {
                builder.setIcon(R.drawable.ic_happy_face)
                        .setMessage(getString(R.string.workout_day_positive));
            } else {
                builder.setIcon(R.drawable.ic_sad_face)
                        .setMessage(getString(R.string.workout_day_negative));
            }
        } else {
            builder.setIcon(R.drawable.ic_confused_face)
                    .setMessage(getString(R.string.workout_day_neutral));
        }

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void saveWorkoutDay(int isWorkoutDone) {
        WorkoutDayBody workoutDayBody = new WorkoutDayBody(isWorkoutDone);
        calendarApiClient.saveWorkoutDay(sharedPref.getUserToken(getContext()), workoutDayBody).enqueue(new Callback<WorkoutDay>() {
            @Override
            public void onResponse(Call<WorkoutDay> call, Response<WorkoutDay> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case ERROR_ACTIVE_REST_DAY:
                            case ERROR_REST_DAY:
                            case ERROR_WORKOUT_DAY:
                                helper.showAlert(getContext(), null, response.body().getMessage(), 0);
                                break;
                            case SUCCESS_WORKOUT_DAY:
                                timestampList.add(response.body().getWorkoutDayData().getWorkoutDate().getTimestamp());
                                workoutDateList.add(response.body().getWorkoutDayData().getWorkoutDate().getFormattedDate());
                                isWorkoutDoneList.add(response.body().getWorkoutDayData().isWorkoutDone());
                                helper.showAlert(getContext(), null, response.body().getMessage(), R.drawable.ic_calendar);
                                saveWorkoutDay.setEnabled(false);
                                break;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<WorkoutDay> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }

    private void checkActiveRestDay(Call<RestDay> call) {
        call.enqueue(new Callback<RestDay>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<RestDay> call, Response<RestDay> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_ACTIVE_REST_DAY:
                                hasRestDay = true;
                                break;
                            case ERROR_ACTIVE_REST_DAY:
                                showAlertForNoRestDay();
                                break;
                        }
                        isRestDayRequestDone = true;
                    }
                }
            }

            @Override
            public void onFailure(Call<RestDay> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }

    private void getUserCalendar(Call<UserCalendar> call) {
        call.enqueue(new Callback<UserCalendar>() {
            @Override
            public void onResponse(Call<UserCalendar> call, Response<UserCalendar> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case ERROR_USER_DATA:
                            case ERROR_GET_USER_CALENDAR:
                                if (!call.isCanceled()) {
                                    helper.showAlert(getContext(), null, response.body().getMessage(), 0);
                                }
                                break;
                            case SUCCESS_GET_USER_CALENDAR:
                                timestampList.clear();
                                workoutDateList.clear();
                                isWorkoutDoneList.clear();
                                for (int i = 0; i < response.body().getWorkoutDayData().size(); i++) {
                                    timestampList.add(response.body().getWorkoutDayData().get(i).getWorkoutDate().getTimestamp());
                                    workoutDateList.add(response.body().getWorkoutDayData().get(i).getWorkoutDate().getFormattedDate());
                                    isWorkoutDoneList.add(response.body().getWorkoutDayData().get(i).isWorkoutDone());
                                }
                                userCalendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                                    @Override
                                    public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {
                                        if (timestampList.contains(getClickedTimestamp(year, month, day))) {
                                            int index = timestampList.indexOf(getClickedTimestamp(year, month, day));
                                            showAlertForForWorkoutDate(workoutDateList.get(index), isWorkoutDoneList.get(index));
                                        } else {
                                            showAlertForForWorkoutDate(getClickedDate(year, month, day), null);
                                        }
                                    }
                                });
                                break;
                        }
                        isCalendarRequestDone = true;
                    }
                }
            }

            @Override
            public void onFailure(Call<UserCalendar> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }

    private void checkTodayWorkout(Call<WorkoutDay> call) {
        call.enqueue(new Callback<WorkoutDay>() {
            @Override
            public void onResponse(Call<WorkoutDay> call, Response<WorkoutDay> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        switch (response.body().getType()) {
                            case SUCCESS_TODAY_WORKOUT:
                                saveWorkoutDay.setEnabled(false);
                                isButtonEnabled = false;
                                break;
                            case ERROR_TODAY_WORKOUT:
                                saveWorkoutDay.setEnabled(true);
                                isButtonEnabled = true;
                                break;
                        }
                        progressBarCalendar.setVisibility(View.GONE);
                        calendarLayout.setVisibility(View.VISIBLE);
                        isWorkoutRequestDone = true;
                        swipeLayout.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<WorkoutDay> call, Throwable t) {
                if (!call.isCanceled()) {
                    helper.showAlert(getContext(), null, t.getMessage(), 0);
                }
            }
        });
    }

    @SuppressLint("SimpleDateFormat")
    private long getClickedTimestamp(int year, int month, int day) {
        String clickedDate = year + "-" + (month + 1) + "-" + day;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        Date date = null;
        try {
            date = format.parse(clickedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        assert date != null;
        return date.getTime();
    }

    @SuppressLint("SimpleDateFormat")
    private String getClickedDate(int year, int month, int day) {
        String clickedDate = year + "-" + (month + 1) + "-" + day;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        Date date = null;
        try {
            date = format.parse(clickedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.mm.yyyy.");
        return dateFormat.format(date);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        restDayCall.cancel();
        userCalendarCall.cancel();
        workoutDayCall.cancel();
    }
}
