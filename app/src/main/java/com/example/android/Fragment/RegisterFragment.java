package com.example.android.Fragment;

import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.example.android.API.ResReq.UserFactory;
import com.example.android.Common.UserCommon;
import com.example.android.R;
import com.google.android.material.textfield.TextInputEditText;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.register_fragment)
class RegisterFragment extends Fragment {

    @ViewById
    TextInputEditText registerUsername;
    @ViewById
    TextInputEditText registerEmail;
    @ViewById
    TextInputEditText registerPassword;
    @ViewById
    TextInputEditText registerConfirmPassword;
    @ViewById
    Button registerButton;

    private UserFactory userFactory;
    private UserCommon userCommon;

    private boolean validUsername = false;
    private boolean validEmail = false;
    private boolean validPassword = false;
    private boolean validConfirmPassword = false;
    private boolean passwordsMatch = false;

    @AfterViews
    void init() {
        userFactory = new UserFactory();
        userCommon = new UserCommon();
        registerButton.setEnabled(false);
    }

    @Click(R.id.register_button)
    void registerButton() {
        if (validUsername &&
                validEmail &&
                validPassword &&
                validConfirmPassword &&
                userCommon.arePasswordsValid(userCommon.getPassword(registerPassword),
                        userCommon.getConfirmPassword(registerConfirmPassword))) {
            userFactory.registerUser(
                    getActivity(),
                    userCommon.getUsername(registerUsername),
                    userCommon.getEmail(registerEmail),
                    userCommon.getPassword(registerPassword),
                    userCommon.getConfirmPassword(registerConfirmPassword)
            );
        }
    }

    @AfterTextChange(R.id.register_username)
    void afterUsernameChange() {
        if (!userCommon.getUsername(registerUsername).isEmpty() &&
                userCommon.isUsernameValid(userCommon.getUsername(registerUsername))) {
            validUsername = true;
            registerUsername.setError(null);
        } else {
            validUsername = false;
            registerUsername.setError(getString(R.string.username_error_message));
        }
    }

    @AfterTextChange(R.id.register_email)
    void afterEmailChange() {
        if (!userCommon.getEmail(registerUsername).isEmpty() &&
                userCommon.isEmailValid(registerEmail)) {
            validEmail = true;
            registerEmail.setError(null);
        } else {
            validEmail = false;
            registerEmail.setError(getString(R.string.email_error_message));
        }
    }

    @AfterTextChange(R.id.register_password)
    void afterPasswordChange() {
        if (!userCommon.getPassword(registerPassword).isEmpty() &&
                userCommon.isPasswordValid(userCommon.getPassword(registerPassword))) {
            validPassword = true;
            registerPassword.setError(null);
        } else {
            validPassword = false;
            registerPassword.setError(getString(R.string.password_error_message));
        }
    }

    @AfterTextChange(R.id.register_confirm_password)
    void afterConfirmPasswordChange() {
        if (!userCommon.getConfirmPassword(registerConfirmPassword).isEmpty() &&
                userCommon.isConfirmPasswordValid(userCommon.getConfirmPassword(registerConfirmPassword))) {
            if (!userCommon.getPassword(registerPassword).equals(userCommon.getConfirmPassword(registerConfirmPassword))) {
                validConfirmPassword = false;
                registerConfirmPassword.setError(getString(R.string.confirm_password_error_message));
            } else {
                validConfirmPassword = true;
                passwordsMatch = true;
                registerConfirmPassword.setError(null);
            }
        } else {
            validConfirmPassword = false;
            registerConfirmPassword.setError(getString(R.string.password_error_message));
        }
    }

    @AfterTextChange({R.id.register_username, R.id.register_email, R.id.register_password, R.id.register_confirm_password})
    void afterAllInputsChange() {
        if (validUsername && validEmail && passwordsMatch) {
            registerButton.setEnabled(true);
        } else {
            registerButton.setEnabled(false);
        }
    }
}
