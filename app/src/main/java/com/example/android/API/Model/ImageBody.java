package com.example.android.API.Model;

public class ImageBody {
    private final String imageFileName;
    private final String encodedImageData;
    private final int isProfileImage;

    public ImageBody(String imageFileName, String encodedImageData, int isProfileImage) {
        this.imageFileName = imageFileName;
        this.encodedImageData = encodedImageData;
        this.isProfileImage = isProfileImage;
    }
}
