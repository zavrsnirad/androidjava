package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSearchData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("imageFileName")
    @Expose
    private String imageFileName;

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getImageFileName() {
        return imageFileName;
    }
}
