package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageData {
    @SerializedName("imageFileName")
    @Expose
    private String imageFileName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("dateData")
    @Expose
    private DateData dateData;
    @SerializedName("profileImageFileName")
    @Expose
    private String profileImageFileName;

    public String getImageFileName() {
        return imageFileName;
    }

    public String getDescription() {
        return description;
    }

    public DateData getDateData() {
        return dateData;
    }

    public String getProfileImageFileName() {
        return profileImageFileName;
    }
}
