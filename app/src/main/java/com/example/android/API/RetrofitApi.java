package com.example.android.API;

import com.example.android.API.Request.CalendarInterface;
import com.example.android.API.Request.ImageInterface;
import com.example.android.API.Request.RestDayInterface;
import com.example.android.API.Request.UserInterface;

public class RetrofitApi {

    static final String BASE_URL = "http://ngrok.workoutposse.tk/api/";
    public static final String WEB_URL = "http://ngrok.workoutposse.tk/";

    public static UserInterface getUserApi() {
        return RetrofitClient.getClient().create(UserInterface.class);
    }

    public static ImageInterface getImageApi() {
        return RetrofitClient.getClient().create(ImageInterface.class);
    }

    public static RestDayInterface getRestDayApi() {
        return RetrofitClient.getClient().create(RestDayInterface.class);
    }

    public static CalendarInterface getCalendarApi() {
        return RetrofitClient.getClient().create(CalendarInterface.class);
    }
}
