package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowingData {
    @SerializedName("userFollowed")
    @Expose
    private Integer userFollowed;
    @SerializedName("userFollowing")
    @Expose
    private String userFollowing;

    public Integer getUserFollowed() {
        return userFollowed;
    }

    public void setUserFollowed(Integer userFollowed) {
        this.userFollowed = userFollowed;
    }

    public String getUserFollowing() {
        return userFollowing;
    }

    public void setUserFollowing(String userFollowing) {
        this.userFollowing = userFollowing;
    }
}
