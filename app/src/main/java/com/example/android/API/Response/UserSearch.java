package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserSearch {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userData")
    @Expose
    private UserData userData;
    @SerializedName("data")
    @Expose
    private ArrayList<UserSearchData> userSearchData;

    public ArrayList<UserSearchData> getUserSearchData() {
        return userSearchData;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public UserData getUserData() {
        return userData;
    }
}
