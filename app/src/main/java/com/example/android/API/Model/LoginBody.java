package com.example.android.API.Model;

public class LoginBody {
    private final String username;
    private final String password;

    public LoginBody(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
