package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Register {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userData")
    @Expose
    private RegisterData userData;

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public RegisterData getUserData() {
        return userData;
    }
}
