package com.example.android.API.Request;

import com.example.android.API.Model.RestDayBody;
import com.example.android.API.Response.AllRestDays;
import com.example.android.API.Response.RestDay;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface RestDayInterface {

    @POST("auth/user/create_rest_day")
    Call<RestDay> createRestDay(@Header("Authorization") String token, @Body RestDayBody restDayBody);

    @GET("auth/user/active_rest_day")
    Call<RestDay> getActiveRestDay(@Header("Authorization") String token);

    @GET("auth/user/all_rest_days")
    Call<AllRestDays> getAllRestDays(@Header("Authorization") String token);
}
