package com.example.android.API.Request;

import com.example.android.API.Response.Delete;
import com.example.android.API.Response.UserImages;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ImageInterface {

    @Multipart
    @POST("auth/user/image_upload")
    Call<com.example.android.API.Response.Image> uploadUserImage(
            @Header("Authorization") String token,
            @Part MultipartBody.Part imageFile,
            @Part("imageFileName") RequestBody imageFileName,
            @Part("isProfileImage") int isProfileImage,
            @Part("description") RequestBody description
    );

    @Multipart
    @POST("auth/user/image_upload")
    Call<com.example.android.API.Response.Image> uploadProfileImage(
            @Header("Authorization") String token,
            @Part MultipartBody.Part imageFile,
            @Part("imageFileName") RequestBody imageFileName,
            @Part("isProfileImage") int isProfileImage,
            @Part("description") String description
    );

    @GET("auth/user/user_images/{username}")
    Call<UserImages> getUserImages(@Header("Authorization") String token, @Path("username") String user);

    @GET("auth/user/user_profile_image/{username}")
    Call<UserImages> getUserProfileImage(@Header("Authorization") String token, @Path("username") String user);

    @DELETE("auth/user/delete_image")
    Call<Delete> deleteImage(@Header("Authorization") String token, @Query("fileName") String imagePath);
}
