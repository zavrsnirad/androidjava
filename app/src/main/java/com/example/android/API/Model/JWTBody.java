package com.example.android.API.Model;

public class JWTBody {
    private final String refresh_token;

    public JWTBody(String refresh_token) {
        this.refresh_token = refresh_token;
    }
}
