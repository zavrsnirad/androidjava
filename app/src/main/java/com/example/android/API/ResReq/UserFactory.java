package com.example.android.API.ResReq;

import android.annotation.SuppressLint;
import android.content.Context;

import com.example.android.API.Model.JWTBody;
import com.example.android.API.Model.LoginBody;
import com.example.android.API.Model.RegisterBody;
import com.example.android.API.Request.UserInterface;
import com.example.android.API.Response.Login;
import com.example.android.API.Response.Register;
import com.example.android.API.RetrofitApi;
import com.example.android.Activity.AuthenticatedActivity_;
import com.example.android.Activity.AuthenticationActivity;
import com.example.android.Common.Helper;
import com.example.android.R;
import com.example.android.Util.SharedPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.android.Common.ResponseTypes.ERROR_REGISTER;
import static com.example.android.Common.ResponseTypes.SUCCESS_REGISTER;

@SuppressLint("Registered")
public class UserFactory extends Helper {

    private SharedPref sharedPref = new SharedPref();
    private UserInterface userApiClient = RetrofitApi.getUserApi();

    public void loginUser(final AuthenticationActivity activity, String username, String password) {
        LoginBody loginBody = new LoginBody(username, password);
        userApiClient.loginCheck(loginBody).enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        sharedPref.saveLoginData(activity, response.body().getToken(), response.body().getRefreshToken());
                        AuthenticatedActivity_.intent(activity).start();
                        activity.finish();
                    }
                } else {
                    showAlert(activity, null, response.message(), 0);
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                showAlert(activity, null, t.getMessage(), 0);
            }
        });
    }

    public void registerUser(final Context context, String username, String email, String password, String confirmPassword) {
        RegisterBody registerBody = new RegisterBody(username, email, password, confirmPassword);
        userApiClient.register(registerBody).enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getType() != null) {
                        getResponseDataFromType(context, response.body().getType(), response);
                    }
                } else {
                    showAlert(context, null, response.message(), 0);
                }
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
                showAlert(context, null, t.getMessage(), 0);
            }
        });
    }

    public void refreshToken(final Context context, final String refreshToken) {
        JWTBody jwtBody = new JWTBody(refreshToken);
        userApiClient.refreshToken(jwtBody).enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        sharedPref.saveNewToken(context, response.body().getToken());
                    }
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                refreshToken(context, refreshToken);
            }
        });
    }

    private void getResponseDataFromType(Context context, String type, Response response) {
        Register register;
        switch (type) {
            case SUCCESS_REGISTER:
                register = (Register) response.body();
                if (register != null) {
                    AuthenticationActivity activity = (AuthenticationActivity) context;
                    loginUser(activity, register.getUserData().getUsername(), register.getUserData().getPassword());
                    sharedPref.setEnabledDailyNotification(context, true);
                } else {
                    showAlert(context, null, context.getString(R.string.no_response_data), 0);
                }
                break;
            case ERROR_REGISTER:
                register = (Register) response.body();
                if (register != null) {
                    showAlert(context, null, register.getMessage(), R.drawable.ic_register);
                } else {
                    showAlert(context, null, context.getString(R.string.no_response_data), 0);
                }
                break;
            default:
                showAlert(context, null, context.getString(R.string.not_valid), R.drawable.ic_image);
                break;
        }
    }
}