package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HomeImage {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userData")
    @Expose
    private Object userData;
    @SerializedName("data")
    @Expose
    private ArrayList<HomeImageData> homeImageData;

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public Object getUserData() {
        return userData;
    }

    public ArrayList<HomeImageData> getHomeImageData() {
        return homeImageData;
    }
}
