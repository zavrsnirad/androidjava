package com.example.android.API.Request;

import com.example.android.API.Model.FollowBody;
import com.example.android.API.Model.JWTBody;
import com.example.android.API.Model.LoginBody;
import com.example.android.API.Model.RegisterBody;
import com.example.android.API.Response.Delete;
import com.example.android.API.Response.HomeImage;
import com.example.android.API.Response.Login;
import com.example.android.API.Response.Mail;
import com.example.android.API.Response.Register;
import com.example.android.API.Response.UserFollowing;
import com.example.android.API.Response.UserSearch;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserInterface {
    @POST("login_check")
    Call<Login> loginCheck(@Body LoginBody login);

    @POST("register")
    Call<Register> register(@Body RegisterBody register);

    @POST("token/refresh")
    Call<Login> refreshToken(@Body JWTBody jwtBody);

    @GET("auth/user/search_users/{searchTerms}")
    Call<UserSearch> searchUsers(@Header("Authorization") String token, @Path("searchTerms") String searchTerms);

    @POST("auth/user/follow")
    Call<UserFollowing> followUser(@Header("Authorization") String token, @Body FollowBody followBody);

    @DELETE("auth/user/unfollow/{userId}")
    Call<UserFollowing> unfollowUser(@Header("Authorization") String token, @Path("userId") Integer userId);

    @GET("auth/user/following/{username}")
    Call<UserFollowing> getFollowingUsers(@Header("Authorization") String token, @Path("username") String username);

    @GET("auth/user/followers/{username}")
    Call<UserFollowing> getFollowers(@Header("Authorization") String token, @Path("username") String username);

    @GET("auth/user/followed_users_images")
    Call<HomeImage> getFollowingUsersImages(@Header("Authorization") String token);

    @DELETE("auth/user/delete_user")
    Call<Delete> deleteUser(@Header("Authorization") String token);

    @GET("auth/user/send_notice/{userId}")
    Call<Mail> sendNotice(@Header("Authorization") String token, @Path("userId") int userId);
}
