package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllRestDays {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userData")
    @Expose
    private UserData userData;
    @SerializedName("data")
    @Expose
    private ArrayList<RestDayData> restDayData;

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public UserData getUserData() {
        return userData;
    }

    public ArrayList<RestDayData> getRestDayData() {
        return restDayData;
    }
}
