package com.example.android.API.Model;

public class WorkoutDayBody {
    private final int isWorkoutDone;

    public WorkoutDayBody(int isWorkoutDone) {
        this.isWorkoutDone = isWorkoutDone;
    }
}
