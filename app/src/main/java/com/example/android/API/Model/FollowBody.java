package com.example.android.API.Model;

public class FollowBody {
    private final Integer userFollowed;

    public FollowBody(Integer userFollowed) {
        this.userFollowed = userFollowed;
    }
}
