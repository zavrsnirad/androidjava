package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeImageData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("imageFileName")
    @Expose
    private String imageFileName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("profileImageFileName")
    @Expose
    private String profileImageFileName;

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getImageFileName() {
        return imageFileName;
    }

    public String getDescription() {
        return description;
    }

    public String getProfileImageFileName() {
        return profileImageFileName;
    }
}
