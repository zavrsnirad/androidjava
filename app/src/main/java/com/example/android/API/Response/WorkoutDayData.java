package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkoutDayData {
    @SerializedName("workoutDate")
    @Expose
    private DateData workoutDate;
    @SerializedName("isWorkoutDone")
    @Expose
    private boolean isWorkoutDone;

    public DateData getWorkoutDate() {
        return workoutDate;
    }

    public boolean isWorkoutDone() {
        return isWorkoutDone;
    }
}
