package com.example.android.API.Request;

import com.example.android.API.Model.WorkoutDayBody;
import com.example.android.API.Response.UserCalendar;
import com.example.android.API.Response.WorkoutDay;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CalendarInterface {

    @POST("auth/user/save_workout_day")
    Call<WorkoutDay> saveWorkoutDay(@Header("Authorization") String token, @Body WorkoutDayBody workoutDayBody);

    @GET("auth/user/get_today_workout")
    Call<WorkoutDay> getTodayWorkout(@Header("Authorization") String token);

    @GET("auth/user/user_calendar/{username}")
    Call<UserCalendar> getUserCalendar(@Header("Authorization") String token, @Path("username") String username);
}
