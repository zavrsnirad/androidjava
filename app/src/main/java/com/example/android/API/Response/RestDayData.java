package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RestDayData {
    @SerializedName("restDay")
    @Expose
    private String restDay;
    @SerializedName("activeFrom")
    @Expose
    private DateData activeFrom;
    @SerializedName("activeTo")
    @Expose
    private DateData activeTo;

    public String getRestDay() {
        return restDay;
    }

    public DateData getActiveFrom() {
        return activeFrom;
    }

    public DateData getActiveTo() {
        return activeTo;
    }
}
