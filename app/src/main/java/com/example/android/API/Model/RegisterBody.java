package com.example.android.API.Model;

public class RegisterBody {
    private final String username;
    private final String email;
    private final String plainPassword;
    private final String plainConfirmPassword;

    public RegisterBody(String username, String email, String plainPassword, String plainConfirmPassword) {
        this.username = username;
        this.email = email;
        this.plainPassword = plainPassword;
        this.plainConfirmPassword = plainConfirmPassword;
    }
}
