package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserFollowing {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userData")
    @Expose
    private UserData userData;
    @SerializedName("data")
    @Expose
    private ArrayList<FollowingData> followingData;

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public UserData getUserData() {
        return userData;
    }

    public ArrayList<FollowingData> getFollowingData() {
        return followingData;
    }
}
