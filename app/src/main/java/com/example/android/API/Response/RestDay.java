package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RestDay {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userData")
    @Expose
    private UserData userData;
    @SerializedName("data")
    @Expose
    private RestDayData restDayData;

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public UserData getUserData() {
        return userData;
    }

    public RestDayData getRestDayData() {
        return restDayData;
    }
}
