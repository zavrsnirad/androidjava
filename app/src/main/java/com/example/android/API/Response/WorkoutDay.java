package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkoutDay {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userData")
    @Expose
    private UserData userData;
    @SerializedName("data")
    @Expose
    private WorkoutDayData workoutDayData;

    public WorkoutDayData getWorkoutDayData() {
        return workoutDayData;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public UserData getUserData() {
        return userData;
    }
}
