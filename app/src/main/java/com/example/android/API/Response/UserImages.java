package com.example.android.API.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserImages {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userData")
    @Expose
    private ArrayList<UserData> userData;
    @SerializedName("data")
    @Expose
    private ArrayList<ImageData> imageData;

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<UserData> getUserData() {
        return userData;
    }

    public ArrayList<ImageData> getImageData() {
        return imageData;
    }
}
